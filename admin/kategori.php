<H2>Kategori</H2>
<div class="panel panel-default">
    <div class="panel-body">
    <div class="panel-heading">
        <a href="index.php?halaman=tambahkategori" class="btn btn-primary">Tambah Kategori</a>
    </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-category">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Kategori</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $nomor = 1;
                        $ambil = $koneksi->query("SELECT * FROM kategori");
                        while ($pecah = $ambil->fetch_assoc()) {
                    ?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['nama_kategori']; ?></td>
                        <td>
                            <a href="index.php?halaman=hapuskategori&id=<?php echo $pecah['id_kategori']; ?>"
                                class="btn-danger btn">Hapus</a>
                            <a href="index.php?halaman=ubahkategori&id=<?php echo $pecah['id_kategori']; ?>"
                                class="btn btn-warning">Ubah</a>
                        </td>
                    </tr>
                    <?php
						$nomor++;
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>