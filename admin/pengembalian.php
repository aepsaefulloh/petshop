<H2>Permintaan Retur / Pengembalian</H2>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" id="table-retur">
				<thead>
					<tr>
						<th>No</th>
						<th>No. Order</th>
						<th>Subjek</th>
						<th>Keterangan</th>
						<th>Foto</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$getRetur = $koneksi->query("SELECT * FROM retur");
					$no = 1;
					while ($retur = $getRetur->fetch_assoc()) :
						$no_order = $koneksi->query("SELECT no_order FROM pembelian WHERE idpembelian = {$retur['idpembelian']}")->fetch_assoc();
					?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $no_order['no_order'] ?></td>
							<td><?= $retur['subjek'] ?></td>
							<td><?= $retur['keterangan'] ?></td>
							<td>
								<a href="../fotoretur/<?php echo $retur['foto'] ?>" data-lightbox="image-<?php echo $no ?>">
									<img src="../fotoretur/<?php echo $retur['foto']; ?>" width="100">
								</a>
							</td>
						</tr>
					<?php
					endwhile;
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>