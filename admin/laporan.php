<!-- <pre><?php echo print_r($status) ?></pre> -->

<H2>Laporan Penjualan</H2>
<div class="panel panel-default">
    <div class="panel-heading">
        Laporan Penjualan
    </div>
    <div class="panel-body">
        <form method="post" enctype="multipart/form-data">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Dari Tanggal</label>
                    <input type="date" class="form-control" name="start" placeholder="dd/mm/yyyy" required=""
                        value="<?php $_POST['start'] ? $_POST['start'] : '-';?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Sampai Tanggal</label>
                    <input type="date" class="form-control" name="end" placeholder="dd/mm/yyyy" required=""
                        value="<?php $_POST['end']?$_POST['end']:'-';?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <?php
			       	    if($_POST['status']=='0'){
                           echo "<option value='all'> All </option> 
								<option value='1'>Payment Complete</option>
								<option value='0' selected>Waiting Payment</option>";
                          
			       	    }else if ($_POST['status']=='1'){
			       	    	echo "<option value='all'> All </option> 
								<option value='1' selected>Payment Complete</option>
								<option value='0'>Waiting Payment</option>";

			       	    }else{
                             echo "<option value='all' selected> All </option> 
								<option value='1'>Payment Complete</option>
								<option value='0'>Waiting Payment</option>";

			       	    } ?>

                    </select>

                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary btn-block" type="submit" name="cari" style="margin-top:1.5em"><i
                        class="fas fa-search"></i> Cari</button>
            </div>
        </form>
    </div>
</div>
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css" rel="stylesheet" />

<style type="text/css">
.buttons-pdf {
    background-color: red;
}
</style>


<?php
    if (isset($_POST['cari'])) 
    {
      # code...
    	$start=date('Y-m-d', strtotime($_POST['start']));
    	$end = date('Y-m-d', strtotime($_POST['end']));
    	$status=$_POST['status'];
        $ambil = $koneksi->query("SELECT * FROM pembelian WHERE tanggalpembelian between '$start' and '$end' and pembelian.status='$status'");
        $totaldata = $ambil->num_rows;
        if ($totaldata > 0) {  ?>



<H3>Laporan Penjualan </H3>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-laporan">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal </th>
                        <th>Nama Pelanggan / No. Order</th>
                        <th>Nama Produk</th>
                        <th>Jumlah </th>
                        <th>Harga</th>
                        <th>Sub Total</th>


                    </tr>
                </thead>
                <tbody>
                    <?php
                        // $getStatus = $koneksi->query("SELECT * FROM pembelian JOIN pembelianproduk ON pembelian.idpembelian = pembelianproduk.idpembelian");
                        // $status = mysqli_fetch_array($getStatus);

						$nomor = 1;
						$grandtotal = 0;
						$ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pembelian.idppelanggan = pelanggan.idppelanggan JOIN pembelianproduk ON pembelianproduk.idpembelian=pembelian.idpembelian JOIN produk on produk.idproduk=pembelianproduk.idproduk WHERE pembelian.tanggalpembelian between '$start' and '$end' and pembelian.status='$status' ORDER BY pembelian.idpembelian  desc");
						while ($pecah = $ambil->fetch_assoc()) {
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['tanggalpembelian']; ?></td>
                        <td><?php echo $pecah['namapelanggan']; ?> / <?php echo $pecah['no_order'] ?></td>
                        <td><?php echo $pecah['namaproduk']; ?></td>
                        <td>
                            <center><?php echo $pecah['jumlah']; ?></center>
                        </td>
                        <td><label style="float:right">Rp. <?php echo number_format($pecah['harga']); ?></label></td>
                        <td><label style="float:right">Rp.
                                <?php echo number_format($pecah['harga']*$pecah['jumlah']); ?></label></td>


                    </tr>
                    <?php
                        $grandtotal += ($pecah['harga']*$pecah['jumlah']);
						$nomor++;
						}
					?>

                    <tr>
                        <td>Grand Total</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td><label style="float:right">Rp. <?php echo number_format($grandtotal); ?></label></td>


                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>


<?php
         
        }else{
            echo "<div class='alert alert-danger'> Data Kosong..</div>";
            
        }
    }

 ?>