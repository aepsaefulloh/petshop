<H2>Detail Pembelian</H2>

<?php
$ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pembelian.idppelanggan=pelanggan.idppelanggan WHERE pembelian.idpembelian='$_GET[id]'");
$detail = $ambil->fetch_assoc();
?>
<!-- <pre><?php print_r($detail); ?></pre> -->

<form method="post">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Produk / Kategori</th>
                <th>Harga</th>
                <th>Jumlah Barang</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <?php
			$nomor = 1;
		    ?>
            <?php 
			$ambil = $koneksi->query("SELECT * FROM pembelianproduk JOIN produk ON pembelianproduk.idproduk = produk.idproduk WHERE pembelianproduk.idpembelian = '$_GET[id]'");
			while ($pecah = $ambil->fetch_assoc()) {
				# code...
		?>
            <!-- <pre><?php print_r($pecah); ?></pre> -->

            <tr>
                <td><?php echo $nomor; ?></td>
                <td><?php echo $pecah['namaproduk']; ?> / <?php echo $pecah['nama_kategori']?></td>
                <td>Rp. <?php echo number_format($pecah['hargaproduk']); ?></td>
                <td><?php echo $pecah['jumlah']; ?></td>
                <td>
                    Rp. <?php 
					echo number_format($pecah['hargaproduk'] * $pecah['jumlah']);
				?>
                </td>

            </tr>
            <?php
			$nomor++;
			}
		?>
            <tr>
                <td colspan="3">
                </td>
                <td>
                    Ongkir :
                </td>
                <td>
                    Rp. <?php echo number_format($detail['tarif']); ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
                <td>
                    <strong>TOTAL PEMBAYARAN :</strong>
                </td>
                <td>
                    <strong>Rp. <?php echo number_format($detail['totalpembelian']); ?></strong>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Username</label>
                        <input type="text" class="form-control" value="<?php echo $detail['namapelanggan']; ?>"
                            disabled>
                    </div>
                    <div class="col-md-6">
                        <label>Nomor Order</label>
                        <input type="text" class="form-control" value="<?php echo $detail['no_order']; ?>" disabled>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" value="<?php echo $detail['emailpelanggan']; ?>" disabled>
            </div>
            <div class="form-group">
                <label>Telepon Pelanggan</label>
                <input type="text" class="form-control" value="<?php echo $detail['telppelanggan']; ?>" disabled>
            </div>
            <div class="form-group">
                <label>Status</label>
                <button type="button" class="btn btn-block btn-<?= $trStatusLabel[$detail['status']] ?>"><?php echo $trStatus[$detail['status']]?></button>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Order</label>
                <input type="text" class="form-control" value="<?php echo $detail['tanggalpembelian']; ?>" disabled>
            </div>
            <div class="form-group">
                <label>Alamat Pengirim</label>
                <input type="text" class="form-control" value="<?php echo $detail['alamatpengiriman']; ?>" disabled>
            </div>
            <div class="form-group">
                <label>Kode Pos</label>
                <input type="text" class="form-control" value="<?php echo $detail['postal_code']; ?>" disabled>
            </div>
        </div>

    </div>
    <!-- <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Status</label>
                <div class="radio" style="display:grid;">
                    <?php foreach($trStatus as $k=>$v){?>
                    <label>
                        <input type="radio" name="status" value="<?php echo $k?>"
                            <?php if($status['status']==$k) echo 'checked'?> /> <?php echo $v?>
                    </label>
                    <?php } ?>
                </div>
            </div>
            <button class="btn btn-primary" name="ubah">Ubah Status</button>
        </div>
    </div> -->
</form>

<?php 
if (isset($_POST['ubah'])) 
{ 
	$koneksi->query("UPDATE pembelianproduk SET status='$_POST[status]' WHERE idpembelian='$_GET[id]'");

	echo "<script>alert('Status telah diubah');</script>";
	echo "<script>location='index.php?halaman=pembelian';</script>";
}

?>
<!-- <pre><?php echo print_r($status) ?></pre> -->