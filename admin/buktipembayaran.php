<H2>Produk</H2>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-invoice">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama pelanggan </th>
                        <th>No Order</th>
                        <th>Tanggal Upload</th>
                        <th>Foto</th>
                        <!-- <th>Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
						$nomor = 1;
						$ambil = $koneksi->query("SELECT * FROM invoice");
						while ($pecah = $ambil->fetch_assoc()) {
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['namapelanggan']; ?>
                        </td>
                        <td><?php echo $pecah['no_order']; ?></td>
                        <td><?php echo $pecah['tanggal']; ?></td>

                        <td>
                            <a href="../invoice/<?php echo $pecah['file_name']; ?>"
                                data-lightbox="image-<?php echo $nomor ?>">
                                <img src="../invoice/<?php echo $pecah['file_name']; ?>" width="80">
                            </a>
                        </td>

                        <!-- <td>
                            <a href="index.php?halaman=hapusproduk&id=<?php echo $pecah['idproduk']; ?>"
                                class="btn-danger btn">Hapus</a>
                            <a href="index.php?halaman=ubahbuktipembayaran&id=<?php echo $pecah['id']; ?>"
                                class="btn btn-warning">Ubah</a>
                        </td> -->
                    </tr>
                    <?php
						$nomor++;
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>