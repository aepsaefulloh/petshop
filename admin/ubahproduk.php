<h2>Ubah Produk</h2>
<?php 
$ambil=$koneksi->query("SELECT * FROM produk WHERE idproduk='$_GET[id]'");
$pecah=$ambil->fetch_assoc();
// echo"<pre>";
// print_r($pecah);
// echo"</pre>";
?>
<form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label>Nama Produk</label>
        <input type="text" class="form-control" name="nama" value="<?php echo $pecah ['namaproduk']; ?>" required>
    </div>
    <div class="form-group">
        <div class="form-group">
            <label>Kategori</label>
            <select class="form-control" name="kategori">
                <option selected disabled>---</option>
                <?php 
				$ambil=$koneksi->query("SELECT * FROM kategori");
				while ($getKategory = $ambil->fetch_assoc()) {			
					?>
                <option value="<?php echo $getKategory['id_kategori']?>"
                    <?php if ($getKategory['id_kategori'] == $pecah['id_kategori']) {echo 'selected';}  ?>>
                    <?php echo $getKategory['nama_kategori'] ?>
                </option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label>Harga (Rp)</label>
        <input type="number" class="form-control" name="harga" value="<?php echo $pecah ['hargaproduk']; ?>" required>
    </div>
    <div class="form-group">
        <label>Diskon <small>( desimal aja )</small></label>
        <input type="text" class="form-control" name="discount" value="<?php echo $pecah ['discount']; ?>" required>
    </div>
    <div class="form-group">
        <label>Berat (Gr)</label>
        <input type="number" class="form-control" name="berat" value="<?php echo $pecah ['beratproduk']; ?>" required>
    </div>
    <div class="form-group">
        <label>Stok</label>
        <input type="number" class="form-control" name="stok" value="<?php echo $pecah ['stok']; ?>" required>
    </div>
    <div class="form-group">
        <img src="../fotoproduk/<?php echo $pecah ['fotoproduk'] ?>" width="200">
    </div>
    <div class="form-group">
        <label>Ganti Foto</label>
        <input type="file" name="foto" class="form-control">
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" class="form-control" rows="10">
			<?php 
				echo $pecah ['deskripsiproduk']; 
			?>
		</textarea>
    </div>
    <button class="btn btn-primary" name="ubah">Ubah</button>
</form>

<?php
if (isset($_POST['ubah']))
{
	$namafoto=$_FILES['foto']['name'];
	$lokasifoto=$_FILES['foto']['tmp_name'];

	$id_kategori = $_POST["kategori"];
	$ambil = $koneksi->query("SELECT * FROM kategori WHERE id_kategori = '$id_kategori'");
	$arrKategori = $ambil->fetch_assoc();
    if($_POST['discount'] !== 0 || $_POST['discount'] !== null){
        $discount = ($_POST['discount'] / 100);
    }else{
        $discount = 0;
    }
	$nama_kategori = $arrKategori['nama_kategori'];

    $nama_product = $_POST["nama"];
    $getProduct = $koneksi->query("SELECT * FROM produk WHERE namaproduk = '$nama_product'");
    $arrProduct = $getProduct->num_rows;
  
    if ( $arrProduct == 1 && $pecah['namaproduk'] != $nama_product ) {
        echo "<div class='alert alert-danger'>Nama Produk sudah digunakan</div>";
    }elseif($arrKategori == null ){
        echo "<div class='alert alert-danger'>Isi Kategori Duls</div>";
    }else{
            //jika foto dirubah
        if (!empty($lokasifoto)) {
            # code..
            move_uploaded_file($lokasifoto, "../fotoproduk/$namafoto");
            $koneksi->query("UPDATE produk SET namaproduk='$nama_product',hargaproduk='$_POST[harga]',discount='$discount',beratproduk='$_POST[berat]',stok='$_POST[stok]',fotoproduk='$namafoto',deskripsiproduk='$_POST[deskripsi]' WHERE idproduk='$_GET[id]'");
        }else{
                $koneksi->query("UPDATE produk SET namaproduk='$nama_product',hargaproduk='$_POST[harga]',discount='$discount',beratproduk='$_POST[berat]',stok='$_POST[stok]',deskripsiproduk='$_POST[deskripsi]',nama_kategori='$nama_kategori',id_kategori='$id_kategori' WHERE idproduk='$_GET[id]'");
            }
        echo "<script>alert('Data Produk Berhasil Diubah');</script>";
        echo "<script>location='index.php?halaman=produk';</script>";
        exit();
    }
}
?>