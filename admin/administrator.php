<H2>Daftar Admin</H2>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="index.php?halaman=tambahadministrator" class="btn btn-primary">Tambah Admin</a>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-pelanggan">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
						$nomor = 1;
						$ambil = $koneksi->query("SELECT * FROM admin");
						while ($pecah = $ambil->fetch_assoc()) {	
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['username']; ?></td>
                        <td><?php echo $pecah['namalengkap']; ?></td>
                        <td><?php echo $pecah['email']; ?></td>

                        <td>
                            <a href="index.php?halaman=hapusadministrator&id=<?php echo $pecah['idadmin']; ?>"
                                class="btn btn-danger">Hapus</a>
                            <a href="index.php?halaman=ubahadministrator&id=<?php echo $pecah['idadmin']; ?>"
                                class="btn btn-warning">Ubah</a>
                    </tr>
                    <?php
						$nomor++;
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>