<H2>Produk</H2>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="index.php?halaman=tambahproduk" class="btn btn-primary">Tambah Produk</a>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-product">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Kategori</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						$nomor = 1;
						$ambil = $koneksi->query("SELECT * FROM produk ORDER BY idproduk desc");
						while ($pecah = $ambil->fetch_assoc()) {
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <!-- <td><?php echo $pecah['namaproduk']; ?></td> -->
                        <?php
                            if($pecah['discount'] != 0){
                                echo '<td width="30%">'.'<div class="product_td"><div>'.($pecah["namaproduk"]).'</div>'.'<div class="btn btn-danger btn-xs">Discount</div></td>';
                            }else{
                                echo '<td>'.($pecah["namaproduk"]).'</td>';
                            }
                        ?>
                        <?php
                            if($pecah['discount'] != 0 ){
                                echo '<td>Rp. '.number_format(($pecah["hargaproduk"]) - ($pecah["hargaproduk"]) * $pecah["discount"]).'</td>';
                            }else{
                                echo '<td>Rp. '.number_format($pecah["hargaproduk"]).'</td>';
                            }
                        ?>
                        <td><?php echo $pecah['stok']?></td>
                        <td><?php echo $pecah['nama_kategori']; ?></td>
                        <td>
                            <a href="../fotoproduk/<?php echo $pecah['fotoproduk']; ?>"
                                data-lightbox="image-<?php echo $nomor ?>">
                                <img src="../fotoproduk/<?php echo $pecah['fotoproduk']; ?>" width="80">
                            </a>
                        </td>
                        <td>
                            <a href="index.php?halaman=hapusproduk&id=<?php echo $pecah['idproduk']; ?>"
                                class="btn-danger btn">Hapus</a>
                            <a href="index.php?halaman=ubahproduk&id=<?php echo $pecah['idproduk']; ?>"
                                class="btn btn-warning">Ubah</a>
                        </td>
                    </tr>
                    <?php
						$nomor++;
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<style>
.product_td {
    display: flex;
    justify-content: space-between;
}

@media(max-width:768px) {
    .product_td {
        display: block;
        justify-content: unset;
    }
}
</style>