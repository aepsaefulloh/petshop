<H2>Daftar Pelanggan</H2>
<div class="panel panel-default">
    <!-- <div class="panel-heading">
        <a href="index.php?halaman=tambahpelanggan" class="btn btn-primary">Tambah Data</a>
    </div> -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-pelanggan">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email </th>
                        <th>Telepon</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						$nomor = 1;
						$ambil = $koneksi->query("SELECT * FROM pelanggan");
						while ($pecah = $ambil->fetch_assoc()) {	
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['namapelanggan']; ?></td>
                        <td><?php echo $pecah['emailpelanggan']; ?></td>
                        <td><?php echo $pecah['telppelanggan']; ?></td>
                    <?php
						$nomor++;
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>