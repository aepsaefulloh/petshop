﻿<?php
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");

if (!isset($_SESSION['admin'])) 
{
    echo "<script>alert('Cie mau masuk');</script>";
    echo "<script>location='login.php';</script>";
    exit();
}

$trStatus = array('0'=>'Waiting Payment', '1'=>'Payment Complete', '2'=>'Payment Failed', '3'=>'Shipping', '4'=>'Complete');
$trStatusLabel = array('0'=>'warning', '1'=>'primary', '2'=>'danger', '3'=>'info', '4'=>'sucess');


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css?<?php echo rand()?>" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css?<?php echo rand()?>" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="assets/css/lightbox.min.css" rel="stylesheet" />

</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" style="font-size:28px;">Palagan Petshop</a>
            </div>
            <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
                <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a>
            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="assets/img/admin.png" class="user-image img-responsive" />
                    </li>
                    <li><a href="index.php"><i class="fas fa-tachometer-alt"></i> Home</a></li>
                    <li><a href="index.php?halaman=administrator"><i class="fas fa-tachometer-alt"></i>
                            Administrator</a>
                    </li>
                    <li><a href="index.php?halaman=kategori"><i class="fa fa-th-list"></i> Kategori</a></li>
                    <li><a href="index.php?halaman=produk"><i class="fas fa-gifts"></i> Produk</a></li>
                    <li><a href="index.php?halaman=pembelian"><i class="fas fa-cart-arrow-down"></i> Pembelian</a></li>
                 
                    <li><a href="index.php?halaman=pelanggan"><i class="fa fa-user"></i> Pelanggan</a></li>
                    <li><a href="index.php?halaman=pengembalian"><i class="fa fa-user"></i> Permintaan Retur</a></li>
                    <li><a href="index.php?halaman=laporan"><i class="fas fa-book"></i> Laporan</a></li>
                    <li><a href="index.php?halaman=logout"><i class="fa fa-fire"></i> Logout</a></li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                            if (isset($_GET['halaman'])) 
                            {
                                # code...
                                if ($_GET['halaman']=="produk") 
                                {
                                    # code...
                                    include 'produk.php';
                                }
                                elseif ($_GET['halaman']=="pembelian") 
                                {
                                    # code...
                                    include 'pembelian.php';
                                }
                                elseif ($_GET['halaman']=="laporan") 
                                {
                                    # code...
                                    include 'laporan.php';
                                }
                                elseif ($_GET['halaman']=="pelanggan") 
                                {
                                    # code...
                                    include 'pelanggan.php';
                                }
                                elseif ($_GET['halaman']=="buktipembayaran") 
                                {
                                    # code...
                                    include 'buktipembayaran.php';
                                }
                                elseif ($_GET['halaman']=="administrator") 
                                {   

                                    include 'administrator.php';
                                }
                                elseif ($_GET['halaman']=="tambahadministrator") 
                                {

                                    include 'tambahadministrator.php';
                                }
                                elseif ($_GET['halaman']=="ubahadministrator") 
                                {

                                    include 'ubahadministrator.php';
                                }
                                elseif ($_GET['halaman']=="hapusadministrator") 
                                {

                                    include 'hapusadministrator.php';
                                }
                                elseif ($_GET['halaman']=="kategori") 
                                {   

                                    include 'kategori.php';
                                }
                                elseif ($_GET['halaman']=="tambahkategori") 
                                {

                                    include 'tambahkategori.php';
                                }
                                elseif ($_GET['halaman']=="ubahkategori") 
                                {

                                    include 'ubahkategori.php';
                                }
                                elseif ($_GET['halaman']=="hapuskategori") 
                                {

                                    include 'hapuskategori.php';
                                }
                                elseif ($_GET['halaman']=="detail") 
                                {
                                    # code...
                                    include 'detail.php';
                                }
                                elseif ($_GET['halaman']=="tambahproduk") 
                                {
                                    # code...
                                    include 'tambahproduk.php';
                                }
                                elseif ($_GET['halaman']=="tambahpelanggan") 
                                {
                                    # code...
                                    include 'tambahpelanggan.php';
                                }
                                elseif ($_GET['halaman']=="hapusproduk") 
                                {
                                    # code...
                                    include 'hapusproduk.php';
                                }
                                elseif ($_GET['halaman']=="hapuspelanggan") 
                                {
                                    # code...
                                    include 'hapuspelanggan.php';
                                }
                                elseif ($_GET['halaman']=="ubahproduk") 
                                {
                                    # code...
                                    include 'ubahproduk.php';
                                }
                                elseif ($_GET['halaman']=="pengembalian") 
                                {
                                    # code...
                                    include 'pengembalian.php';
                                }
                                elseif ($_GET['halaman']=="ubahpelanggan") 
                                {
                                    # code...
                                    include 'ubahpelanggan.php';
                                }
                                elseif ($_GET['halaman']=="logout") 
                                {
                                    # code...
                                    include 'logout.php';
                                }
                            }
                            else 
                            {
                                include 'home.php';
                            }
                ?>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/font-awesome.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js?"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="assets/js/lightbox.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#table-product').dataTable();
        $('#table-pembelian').dataTable();
        $('#table-retur').dataTable();
        $('#table-category').dataTable();
        $('#table-pelanggan').dataTable();
        $('#table-invoice').dataTable();
        $('#table-laporan').dataTable({
             "paging": false,
               "ordering": false,
               "searching": false,
               "info": false,
               dom: 'Bfrtip',
                buttons: [
                 { "extend": 'pdf', "text":'Cetak Pdf',"className": 'btn btn-default', "style": 'float:right' },
                 { "extend": 'print', "text":'Print',"className": 'btn btn-default', "style": 'float:right' }
                ]
        });

        
    });

    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })
    </script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js?<?php echo rand()?>"></script>
    

    <?php if ($_GET['halaman']=="laporan") { ?>

    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>

    <?php } ?>


</body>

</html>