<H2>Tambah Produk</H2>
<form method="post" enctype="multipart/form-data" id="addProduct">
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" value="<?php echo isset($_POST['nama']) ? $_POST['nama'] : ''; ?>" required>
    </div>
    <div class="form-group">
        <div class="form-group">
            <label>Kategori</label>
            <select class="form-control" name="kategori" >
                <option selected disabled>---</option>
                <?php 
				$ambil=$koneksi->query("SELECT * FROM kategori");
				while ($getKategory = $ambil->fetch_assoc()) {			
					?>
                <option value="<?php echo $getKategory['id_kategori']?>"><?php echo $getKategory['nama_kategori'] ?> 
                </option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label>Harga (Rp)</label>
        <input type="number" class="form-control" name="harga" value="<?php echo isset($_POST['harga']) ? $_POST['harga'] : ''; ?>" required>
    </div>
    <div class="form-group">
        <label>Diskon <small>( desimal aja / tidak ada diskon kasih 0 )</small></label>
        <input type="text" class="form-control" name="discount" value="0">
    </div>
    <div class="form-group">
        <label>Berat (Gr)</label>
        <input type="number" class="form-control" name="berat" value="<?php echo isset($_POST['berat']) ? $_POST['berat'] : ''; ?>" required>
    </div>
    <div class="form-group">
        <label>Stok</label>
        <input type="number" class="form-control" name="stok" value="<?php echo isset($_POST['stok']) ? $_POST['stok'] : ''; ?>" required>
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="form-control" name="deskripsi" rows="10" value="<?php echo isset($_POST['deskripsi']) ? $_POST['deskripsi'] : ''; ?>"></textarea>
    </div>
    <div class="form-group">
        <label>Foto</label>
        <input type="file" class="form-control" name="foto" required>
    </div>
    <button class="btn btn-primary" name="save">Simpan</button>
</form>
<?php
if (isset($_POST['save'])) 
{
	
	$nama = $_FILES['foto']['name'];
	$lokasi = $_FILES['foto']['tmp_name'];
	move_uploaded_file($lokasi, "../fotoproduk/".$nama);
    
	$id_kategori = $_POST["kategori"];
	$ambil = $koneksi->query("SELECT * FROM kategori WHERE id_kategori = '$id_kategori'");
	$arrKategori = $ambil->fetch_assoc();
    if($_POST['discount'] != 0 || $_POST['discount'] != null){
        $discount = ($_POST['discount'] / 100);
    }else{
        $discount = 0;
    }
    // print_r($discount);
    // exit();

    // echo $arrKategori;
    
	$nama_kategori = $arrKategori['nama_kategori'];

	$nama_product = $_POST["nama"];

    $getProduct = $koneksi->query("SELECT * FROM produk WHERE namaproduk = '$nama_product'");

    $arrProduct = $getProduct->num_rows;


    if ( $arrProduct == 1) {
        echo "<div class='alert alert-danger'>Nama Produk sudah digunakan</div>";
    }elseif($arrKategori == null ){
        echo "<div class='alert alert-danger'>Isi Kategori Duls</div>";
    }elseif($nama){
        echo "<div class='alert alert-danger'>Masukin poto dulu</div>";
    }else{
        $query = "INSERT INTO produk (namaproduk, hargaproduk, discount, beratproduk, stok, fotoproduk, deskripsiproduk, nama_kategori,id_kategori) VALUES 
        ('$nama_product','$_POST[harga]','$discount','$_POST[berat]','$_POST[stok]','$nama','$_POST[deskripsi]','$nama_kategori','$id_kategori')";
        $results = mysqli_query($koneksi, $query);
        echo "<div class='alert alert-info'>Data Tersimpan</div>";
      echo "<meta http-equiv='refresh' content='1;url=index.php?halaman=produk'>"; 
        exit();
      }
    }
    

?>

<script>
