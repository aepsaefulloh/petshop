<!-- <pre><?php echo print_r($status) ?></pre> -->

<H2>Pembelian</H2>
<div class="panel panel-default">
    
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-pembelian">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Pelanggan / No. Order</th>
                        <th>Tanggal </th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        // $getStatus = $koneksi->query("SELECT * FROM pembelian JOIN pembelianproduk ON pembelian.idpembelian = pembelianproduk.idpembelian");
                        // $status = mysqli_fetch_array($getStatus);

						$nomor = 1;
						$ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pembelian.idppelanggan = pelanggan.idppelanggan ORDER BY idpembelian  desc");
						while ($pecah = $ambil->fetch_assoc()) {
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['namapelanggan']; ?> / <?php echo $pecah['no_order'] ?></td>
                        <td><?php echo $pecah['tanggalpembelian']; ?></td>
                        <td><?php echo $pecah['totalpembelian']; ?></td>
                        <td><a href="index.php?halaman=detail&id=<?php echo $pecah['idpembelian']; ?>">
                                <span class="btn btn-<?= $trStatusLabel[$pecah['status']] ?> btn-xs"><?php echo $trStatus[$pecah['status']]?></span></a>
                        </td>
                        <td>
                            <a href="index.php?halaman=detail&id=<?php echo $pecah['idpembelian']; ?>"
                                class="btn btn-info">Detail</a>
                        </td>
                    </tr>
                    <?php
						$nomor++;
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>