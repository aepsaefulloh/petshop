<?php
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");

if (!isset($_SESSION["pelanggan"])) 
{
	# code...
	echo "<script>alert('Silahkan login');</script>";
	echo "<script>location='login.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Checkout</title>
    <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>

<body>
    <?php include 'menu.php'; ?>
    <section class="konten">
        <div class="container">
            <h1>Keranjang Belanja</h1>
            <hr>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>Harga Produk</th>
                        <th>Jumlah</th>
                        <th>Subharga</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						$nomor = 1;
					?>
                    <?php $totalbelanja = 0; ?>
                    <?php foreach ($_SESSION["keranjang"] as $idproduk => $jumlah): ?>
                    <?php
							$ambil = $koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
							$pecah = $ambil->fetch_assoc();
							$subharga = $pecah["hargaproduk"]*$jumlah;
						?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah["namaproduk"]; ?></td>
                        <td>Rp. <?php echo number_format($pecah["hargaproduk"]); ?></td>
                        <td><?php echo $jumlah; ?></td>
                        <td>Rp. <?php echo number_format($subharga); ?></td>
                    </tr>
                    <?php $nomor++; ?>
                    <?php $totalbelanja+=$subharga; ?>
                    <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4">Total Belanja</th>
                        <th>Rp. <?php echo number_format($totalbelanja) ?></th>
                    </tr>
                </tfoot>
            </table>
            <form method="post">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" readonly value="<?php echo $_SESSION["pelanggan"]["namapelanggan"] ?>"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" readonly value="<?php echo $_SESSION["pelanggan"]["telppelanggan"] ?>"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="id_ongkir">
                            <option value="">Pilihan ongkir</option>
                            <?php
								$ambil = $koneksi->query("SELECT * FROM ongkir");
								while ($perongkir = $ambil->fetch_assoc()) 
								{
									# code...
							?>
                            <option value="<?php echo $perongkir["id_ongkir"] ?>">
                                <?php echo $perongkir['namakota'] ?>
                                Rp. <?php echo number_format($perongkir['tarif']) ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat lengkap</label>
                    <textarea class="form-control" name="alamatpengiriman"
                        placeholder="Masukkan alamat lengkap pengiriman"></textarea>
                </div>
                <button class="btn btn-primary" name="checkout">Checkout</button>
            </form>
            <?php
			if (isset($_POST["checkout"])) 
			{
				# code...
				$idppelanggan = $_SESSION["pelanggan"]["idppelanggan"];
				$id_ongkir = $_POST["id_ongkir"];
				$tanggalpembelian = date("Y-m-d");
				$alamatpengiriman=$_POST['alamatpengiriman'];

				$ambil = $koneksi->query("SELECT * FROM ongkir WHERE id_ongkir = '$id_ongkir'");
				$arrayongkir = $ambil->fetch_assoc();
				$namakota = $arrayongkir['namakota'];
				$tarif = $arrayongkir['tarif'];

				$totalpembelian = $totalbelanja + $tarif;

				$koneksi->query("INSERT INTO pembelian (idppelanggan,id_ongkir,tanggalpembelian,totalpembelian, namakota, tarif, alamatpengiriman) VALUES ('$idppelanggan','$id_ongkir','$tanggalpembelian','$totalpembelian','$namakota','$tarif','$alamatpengiriman')");

				$idpembelibarusan = $koneksi->insert_id;
				foreach ($_SESSION["keranjang"] as $idproduk => $jumlah) 
				{
					$ambil=$koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
					$perproduk = $ambil->fetch_assoc();
					$nama=$perproduk['namaproduk'];
					$harga=$perproduk['hargaproduk'];
					$berat=$perproduk['berat'];

					$subberat=$perproduk['berat']*$jumlah;
					$subharga=$perproduk['hargaproduk']*$jumlah;
					$koneksi->query("INSERT INTO pembelianproduk (idpembelian,idproduk,jumlah,nama,harga,berat,subberat,subharga) VALUES ('$idpembelibarusan','$idproduk','$jumlah','$nama','$harga','$berat','$subberat','$subharga')");
				}

				unset($_SESSION["keranjang"]);

				echo "<script>alert('Pembelian sukses');</script>";
				echo "<script>location='nota.php?id=$idpembelibarusan';</script>";
			}
			?>
        </div>
    </section>
    <!-- <pre><?php print_r($_SESSION['pelanggan']) ?></pre>
	<pre><?php print_r($_SESSION['keranjang']) ?></pre> -->
</body>

</html>