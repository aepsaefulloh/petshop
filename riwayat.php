<?php
session_start();

$koneksi = new mysqli("localhost", "root", "", "petshop");
if (!isset($_SESSION['pelanggan'])) {
	header('location: login.php');
}
$trStatus = array('0' => 'Waiting Payment', '1' => 'Payment Complete', '2' => 'Payment Failed', '3' => 'Shipping', '4' => 'Complete');
$trStatusLabel = array('0' => 'warning', '1' => 'primary', '2' => 'danger', '3' => 'info', '4' => 'sucess');

if (isset($_POST['submitulasan'])) {
	$idpembelian = $_POST['idpembelian'];
	$teksUlasan = $_POST['teks_ulasan'];
	$query = $koneksi->query("INSERT INTO ulasan VALUES('', $idpembelian, '$teksUlasan')");

	if ($query) {
		echo '<script>alert("berhasil menambah ulasan");window.location.reload()</script>';
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Palagan Petshop</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand() ?>">
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand() ?>">

</head>

<body>
	<?php require_once 'include/header.php'; ?>

	<section class="section-cart my-5">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-heading">
						<h1>Riwayat Pembelian</h1>
						<div class="line1"></div>
					</div>
				</div>
				<div class="col-12">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">No.</th>
									<th scope="col">NO ORDER</th>
									<th scope="col">TANGGAL</th>
									<th scope="col">TOTAL</th>
									<th scope="col">STATUS</th>
									<th scope="col">BAYAR</th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								$idPelanggan = $_SESSION['pelanggan']['idppelanggan'];
								$query = $koneksi->query("SELECT * FROM pembelian WHERE idppelanggan='$idPelanggan' ORDER BY idpembelian DESC");
								while ($data = $query->fetch_assoc()) :
								?>
									<tr>
										<td><?= $no++; ?></td>
										<td><b><?= $data['no_order']; ?></b></td>
										<td><?= $data['tanggalpembelian']; ?></td>
										<td>Rp<?= number_format($data['totalpembelian']); ?></td>
										<td>
											<span class="badge bg-<?= $trStatusLabel[$data['status']] ?>">
												<?php echo $trStatus[$data['status']] ?>
											</span>
										</td>
										<td>
											<?php if ($data['status'] == 0) : ?>
												<a class="btn btn-warning btn-sm" href="order-success.php?id=<?= $data['idpembelian'] ?>">Bayar</a>
											<?php elseif ($data['status'] == 1) : ?>
												<button class="btn btn-success btn-sm" disabled>Lunas</button>
											<?php endif; ?>
										</td>
										<td>
											<?php if ($data['status'] == 1) : ?>
												<div class="dropdown">
													<a class="btn btn-secondary dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false"></a>

													<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<li><a class="dropdown-item" href="ulasan.php?id=<?= $data['idpembelian'] ?>">Ulasan & Rating</a></li>
														<li><a class="dropdown-item" href="pengembalian.php?id=<?= $data['idpembelian'] ?>">Pengembalian</a></li>
													</ul>
												</div>
											<?php endif; ?>
										</td>
									</tr>
								<?php
								endwhile;
								?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
		</div>
	</section>

	<?php require_once 'include/footer.php'; ?>

	<script src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>