<?php
session_start();

$koneksi = new mysqli("localhost", "root", "", "petshop");
if (!isset($_SESSION['pelanggan'])) {
	header('location: login.php');
}

if (!isset($_GET['id']) || empty($_GET['id'])) {
	header('location: riwayat.php');
}

$idpembelian = $_GET['id'];
$query = $koneksi->query("SELECT * FROM pembelian WHERE idpembelian = {$idpembelian}");
$retur = $koneksi->query("SELECT * FROM retur WHERE idpembelian = {$idpembelian}");

if ($query->num_rows == 0) {
	header('location: riwayat.php');
}
$datapembelian = $query->fetch_assoc();

if (isset($_POST['submit'])) {
	if(empty($_FILES) || empty($_POST['subjek']) || empty($_POST['keterangan'])) {
		echo '<script>alert("Isi semua form dengan benar!")</script>';
	} else {
		$subjek = $_POST['subjek'];
		$keterangan = $_POST['keterangan'];
		$namaFoto = $_FILES['file']['name'];
		$tempFoto = $_FILES['file']['tmp_name'];
		$ekstensi = pathinfo($namaFoto, PATHINFO_EXTENSION);
		$namaFotoNew = 'retur-' . rand() . '.' . $ekstensi;
		
		if (move_uploaded_file($tempFoto, 'fotoretur/'.$namaFotoNew)){
			$koneksi->query("INSERT INTO retur VALUES('',{$idpembelian},'{$subjek}','{$keterangan}','{$namaFotoNew}')");
			header("location: pengembalian.php?id=".$idpembelian);
		} else {
			echo '<script>alert("Gagal mengajukan pengembalian!")</script>';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Palagan Petshop</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand() ?>">
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand() ?>">

</head>

<body>
	<?php require_once 'include/header.php'; ?>

	<section class="section-cart my-5">
		<div class="container" style="width: 560px;">
			<div class="card">
				<div class="card-header text-center">
					<h5>Pengajuan Pengembalian</h5>
				</div>
				<div class="card-body">
					<?php if ($retur->num_rows > 0): ?>
						<div class="text-center">
							<i class="bi bi-check2-circle" style="font-size: 100px;color: green"></i>
						</div>
						<div class="text-center" style="font-size: 20px;margin: 0 0 20px;font-weight: bold;">Pengajuan retur sedang direview</div>
					<?php else: ?>
					<form action="" method="POST" enctype="multipart/form-data">
						<div class="mb-3">
							<label class="form-label">No. Order</label>
							<input type="text" class="form-control" value="<?= $datapembelian['no_order'] ?>" disabled>
						</div>
						<div class="mb-3">
							<label class="form-label">Subjek</label>
							<input type="text" class="form-control" name="subjek" placeholder="Masukkan Subjek">
						</div>
						<div class="mb-3">
							<label class="form-label">Keterangan</label>
							<textarea name="keterangan" id="" rows="3" class="form-control" placeholder="Masukkan Keterangan"></textarea>
						</div>
						<div class="mb-3">
							<label class="form-label">Foto/Bukti</label>
							<input type="file" name="file" class="form-control">
						</div>
						<div class="mb-3">
							<button type="submit" name="submit" class="btn btn-success w-100 mt-3">Kirim</button>
						</div>
					</form>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

	<?php require_once 'include/footer.php'; ?>

	<script src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>