<?php 
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
// if (!isset($_SESSION['pelanggan'])){
//     header("Location: login.php");
// }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">




</head>

<body>
    <?php require_once 'include/header.php'; ?>
    <?php require_once 'include/floating-wa.php'; ?>

    <section class="section-banner">
        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="assets/img/banner/1.jpg" class="img-fluid" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="assets/img/banner/2.jpg" class="img-fluid" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="assets/img/banner/3.jpg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>

    <!-- rekomendasi -->
    <section class="section-content my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h4>Produk Rekomendasi</h4>

                    </div>
                    <div class="grid-product">
                        <?php 
                           	$limit = 6;
                            $orderBy="WHERE idproduk ORDER BY hit DESC";
                            $getData = mysqli_query($koneksi,"select * from produk $orderBy limit $limit");
                            while($list = mysqli_fetch_array($getData)){
                            ?>
                        <figure class="figure">
                            <?php
                                if($list['discount'] != 0){
                                    echo '<span class="btn btn-sm btn-danger rounded">'.($list['discount'] * 100).'%</span>';
                                }else{
                                    echo '';
                                }
                            ?>
                            <a href="detail.php?id=<?php echo $list["idproduk"]; ?>">
                                <img src="fotoproduk/<?php echo $list['fotoproduk'] ?>"
                                    class="figure-img img-fluid rounded" alt="...">
                                <figcaption class="figure-caption ps-3">
                                    <h3>
                                        <?php echo $list['namaproduk'] ?>
                                    </h3>
                                    <!-- <p>Rp.<?php echo number_format($list['hargaproduk']) ?></p> -->
                                    <?php
                                            if( $list['discount'] != 0){
                                                echo '<del><small>Rp.'. number_format($list['hargaproduk']).'</small></del>';
                                                echo '<p>Rp. '.number_format(($list["hargaproduk"]) - ($list["hargaproduk"]) * $list["discount"]).'</p>';
                                            }else{
                                                echo '<p>Rp. '.number_format($list["hargaproduk"]).'</p>';
                                            }
                                        ?>
                                </figcaption>
                            </a>
                        </figure>
                        <?php } ?>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--konten-->
    <section class="section-content my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h4>Produk Terbaru</h4>
                    </div>
                    <div class="grid-product">
                        <?php 
                           	$limit = 6;
                            $orderBy="WHERE idproduk ORDER BY idproduk DESC";
                            $get_data = mysqli_query($koneksi,"SELECT * FROM produk $orderBy limit $limit ");
                            while($item = mysqli_fetch_array($get_data)){
                                    
                            ?>
                        <figure class="figure">
                            <?php
                                if($item['discount'] != 0){
                                    echo '<span class="btn btn-sm btn-danger rounded m-2">'.($item['discount'] * 100).'%</span>';
                                }else{
                                    echo '';
                                }
                            ?>
                            <a href="detail.php?id=<?php echo $item["idproduk"]; ?>">
                                <img src="fotoproduk/<?php echo $item['fotoproduk'] ?>"
                                    class="figure-img img-fluid rounded" alt="...">
                                <figcaption class="figure-caption ps-3">
                                    <h3>
                                        <?php echo $item['namaproduk'] ?>
                                    </h3>
                                    <?php
                                        if( $item['discount'] != 0){
                                            echo '<del><small>Rp.'. number_format($item['hargaproduk']).'</small></del>';
                                            echo '<p>Rp. '.number_format(($item["hargaproduk"]) - ($item["hargaproduk"]) * $item["discount"]).'</p>';
                                        }else{
                                            echo '<p>Rp. '.number_format($item["hargaproduk"]).'</p>';
                                        }
                                    ?>
                                </figcaption>
                            </a>
                        </figure>
                        <?php } ?>

                    </div>
                </div>
            </div>

        </div>
    </section>


    <?php require_once 'include/footer.php'; ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/swiper-bundle.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script>
    var swiper = new Swiper(".mySwiper", {
        loop: true,
        autoplay: {
            delay: 7000,
            disableOnInteraction: false,
        },
        autoHeight: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
    });
    </script>


</body>

</html>