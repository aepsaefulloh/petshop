<?php 
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
$id_kategori = $_GET["id"];
$ambil = $koneksi->query("SELECT * FROM kategori WHERE id_kategori='$id_kategori'");
$dd = $ambil->fetch_assoc();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop - <?php echo $dd['nama_kategori'] ?></title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">



</head>

<body>
    <?php require_once 'include/header.php'; ?>
    <section class="section-timezone ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 align-self-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $dd['nama_kategori'] ?>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-4 text-center align-self-center">
                    <span><span id="city"></span> | <span id="date"></span> | <span id="time"></span></span>

                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </section>
    <!--konten-->
    <section class="section-content my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h4><?php echo $dd['nama_kategori'] ?></h4>
                    </div>
                    <div class="grid-product">
                        <?php 
                           	$limit = 20;
                            $orderBy="WHERE id_kategori='$id_kategori' ORDER BY idproduk DESC";
                            $getProduct = mysqli_query($koneksi,"select * from produk $orderBy limit $limit");
                            while($item = mysqli_fetch_array($getProduct)){
                            ?>
                        <figure class="figure">
                            <?php
                                if($item['discount'] != 0){
                                    echo '<span class="btn btn-sm btn-danger m-2 rounded">'.($item['discount'] * 100).'%</span>';
                                }else{
                                    echo '';
                                }
                            ?>
                            <a href="detail.php?id=<?php echo $item["idproduk"]; ?>">
                                <img src="fotoproduk/<?php echo $item['fotoproduk'] ?>"
                                    class="figure-img img-fluid rounded" alt="...">
                                <figcaption class="figure-caption ps-3">
                                    <h3>
                                        <?php echo $item['namaproduk'] ?>
                                    </h3>
                                    <!-- <p>Rp.<?php echo number_format($item['hargaproduk']) ?></p> -->
                                    <?php
                                            if( $item['discount'] != 0){
                                                echo '<del><small>Rp.'. number_format($item['hargaproduk']).'</small></del>';
                                                echo '<p>Rp. '.number_format(($item["hargaproduk"]) - ($item["hargaproduk"]) * $item["discount"]).'</p>';
                                            }else{
                                                echo '<p>Rp. '.number_format($item["hargaproduk"]).'</p>';
                                            }
                                        ?>
                                </figcaption>
                            </a>
                        </figure>

                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <?php require_once 'include/footer.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/app.js?<?php echo rand()?>"></script>
</body>

</html>