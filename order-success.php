<?php 
session_start();
// Midtrans
$midtransServerKey = 'SB-Mid-server-cHKKQ6i2aWalQRlu7-S7QuHx';
$midtransClientKey = 'SB-Mid-client-n2Ur0uLGRMVF6wcF';
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
// mistrans
require_once dirname(__FILE__) . '/api/midtrans/Midtrans.php';

$idpembelian = $_GET['id'];
$cekPembayaran = $koneksi->query("SELECT * FROM pembayaran WHERE idpembelian = $idpembelian LIMIT 1");

$snapToken = '';
$pembayaran = null;

$queryPembelian = $koneksi->query("SELECT * FROM pembelian WHERE idpembelian='$_GET[id]'");
$pembelian = $queryPembelian->fetch_assoc();

if($cekPembayaran->num_rows > 0) {
    $pembayaran = $cekPembayaran->fetch_assoc();
    $snapToken = $pembayaran['token_bayar'];
} else {
    // Set your Merchant Server Key
    \Midtrans\Config::$serverKey = $midtransServerKey;
    // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
    \Midtrans\Config::$isProduction = false;
    // Set sanitization on (default)
    \Midtrans\Config::$isSanitized = true;
    // Set 3DS transaction for credit card to true
    \Midtrans\Config::$is3ds = true;
    
    $params = array(
        'transaction_details' => array(
            'order_id' => rand(),
            'gross_amount' => $pembelian['totalpembelian'],
        ),
        'customer_details' => array(
            'first_name' => $_SESSION['pelanggan']['namapelanggan'],
            'email' => $_SESSION['pelanggan']['emailpelanggan'],
            'phone' => $_SESSION['pelanggan']['telppelanggan'],
        ),
        'enabled_payments' => ["bni_va", "bri_va", "bca_va"],
    );
    
    $snapToken = \Midtrans\Snap::getSnapToken($params);
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">
    <script type="text/javascript"
      src="https://app.sandbox.midtrans.com/snap/snap.js"
      data-client-key="<?= $midtransClientKey ?>"></script>


</head>

<body>
    <?php require_once 'include/header.php'; ?>

    <section class="section-order-success my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="main-heading text-md-center">
                        <h3>Store</h3>
                        <p>Thank You | <span id="date"></span> | <span id="time"></p>
                        <div class="line1"></div>
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-12">
                    <div class="text-order text-center">
                        <p>Hi, <?php echo $_SESSION['pelanggan']['namapelanggan']; ?></p>
                        <p>Thank you for your order! Your items will be delivered after the payment has been made.<br>
                            Please make your payment within 24 hours to avoid order cancellation and send the payment
                            receipt to palagan@email.com</p>
                        <div class="item my-5">
                            <p>No Order : <?php echo $pembelian['no_order']; ?></p>
                            <p>Shipping : </p>
                            <ul>
                                <?php 
                                $getProduct = mysqli_query($koneksi,"SELECT nama, jumlah FROM pembelianproduk WHERE idpembelian='$_GET[id]'");
                                while($item = mysqli_fetch_array($getProduct)) { 
                                    echo "<li>".$item['nama'].' x '.$item['jumlah']."</li>";
                                } ?>
                            </ul>
                            <p>Total : Rp. <?php echo number_format($pembelian['totalpembelian']); ?></p>
                        </div>
                        <p>Please settle the payment by transferring it to the following bank account with your order
                            number
                            as the reference. </p>
                        <!-- <p>BRI / 5551-01-018590-53-6 A/N Palagan Petshop</p> -->
                        <?php 
                        if ($pembelian['status'] == 1) : ?>
                        <div class="alert bg-success text-light">
                            <b>Pembayaran Sukses</b>
                        </div>
                        <?php elseif ($pembelian['status'] == 2): ?>
                        <div class="alert bg-danger text-light">
                            <b>Pembayaran Gagal</b>
                        </div>
                        <?php else: ?>
                        <div>
                            <button class="btn btn-success px-4 py-3" id="bayar"><b>Bayar Sekarang</b></button>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="line1"></div>
                </div>
            </div>
        </div>
    </section>

    <?php require_once 'include/footer.php'; ?>

    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/app.js?<?php echo rand()?>"></script>


    <script>
        function prosesBayar(result, callback){
            const data = {
                "id_pembelian": '<?= $_GET['id'] ?>',
                "bank": result.va_numbers ? result.va_numbers[0].bank : 'null',
                "snapToken": '<?= $snapToken ?>',
                "status_code": result.status_code,
                "status": result.transaction_status,
            };

            fetch("api/pembelian.php", {
                method: 'POST',
                body: JSON.stringify(data),
            })
            .then(res => res.json())
            .then(res => callback(res))
        }
        var BayarBtn = document.getElementById('bayar');

      BayarBtn.addEventListener('click', function () {
        window.snap.pay('<?= $snapToken ?>', {
          onSuccess: function(result){
            /* You may add your own implementation here */
            prosesBayar(result, (res) => {
                alert('Pambayaran berhasil!');
                window.location.reload();
            })
          },
          onPending: function(result){
            /* You may add your own implementation here */
            prosesBayar(result, (res) => {
                alert('Silahkan melakukan pembayaran!');
            })
          },
          onError: function(result){
            /* You may add your own implementation here */
            prosesBayar(result, (res) => {
                if (result.status_code == 409 || result.status_code == 200) {
                    alert('pembayaran berhasil!');
                    window.location.reload();
                } else {
                    alert("pembayaran gagal!");
                    window.location.reload();
                }
            })
          },
          onClose: function(){
            /* You may add your own implementation here */
            alert('Membatalkan Pembayaran');
          }
        })
      });
    </script>
</body>

</html>