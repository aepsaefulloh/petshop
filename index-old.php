<?php
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
?>

<!DOCTYPE html>
<html>

<head>
    <title>Palagan Petshop</title>
    <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<style>
body {
    background-image: url(5.jpg);
    background-size: cover;
    background-attachment: fixed;
}
</style>

<body>
    <?php include 'menu.php'; ?>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="pd.jpg" alt="First slide" height="550px" width="100px">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="rc.jpg" alt="Second slide" height="550px" width="100px">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="whiskas.jpg" alt="Third slide" height="550px" width="100px">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div><br>
    <!--konten-->
    <section class="konten">
        <div class="container">
            <h1>Produk Terbaru</h1>
            <div class="row">

                <?php $ambil = $koneksi->query("SELECT * FROM produk"); ?>
                <?php while($perproduk = $ambil->fetch_assoc()){ ?>
                <div class="col-md-3">
                    <div class="thumbnail">
                        <img src="fotoproduk/<?php echo $perproduk['fotoproduk'] ?>" alt="">
                        <div class="Caption">
                            <h3><?php echo $perproduk['namaproduk'] ?></h3>
                            <h5>Rp. <?php echo number_format($perproduk['hargaproduk']) ?></h5>
                            <a href="beli.php?id=<?php echo $perproduk['idproduk'] ?>" class="btn btn-primary">Beli</a>
                            <a href="detail.php?id=<?php echo $perproduk["idproduk"]; ?>"
                                class="btn btn-default">Detail</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Sebelumnya</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Selanjutnya</a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>
</body>

<?php require_once 'include/footer.php'; ?>

</html>