window.setTimeout("waktu()", 1000);

function waktu() {
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes(0) + ":" + today.getSeconds();
    var date = today.toDateString();
    setTimeout("waktu()", 1000);
    document.getElementById('time').innerHTML = time;
    document.getElementById('date').innerHTML = date;
}

$.ajax({
    url: "https://geolocation-db.com/jsonp",
    jsonpCallback: "callback",
    dataType: "jsonp",
    success: function (location) {
        $('#country').html(location.country_name);
        $('#state').html(location.state);
        $('#city').html(location.city);
        $('#latitude').html(location.latitude);
        $('#longitude').html(location.longitude);
        $('#ip').html(location.IPv4);
    }
});

// (function ($) {
//     "use strict";
//     /*=====================
//      05. Image to background js
//      ==========================*/
//     $(".bg-top").parent().addClass('b-top');
//     $(".bg-bottom").parent().addClass('b-bottom');
//     $(".bg-center").parent().addClass('b-center');
//     $(".bg_size_content").parent().addClass('b_size_content');
//     $(".bg-img").parent().addClass('bg-size');
//     $(".bg-img.blur-up").parent().addClass('blur-up lazyload');
//     $(".bg-article.blur-up").parent().addClass('bg-size lazyload');

//     jQuery('.bg-img').each(function () {

//         var el = $(this),
//             src = el.attr('src'),
//             parent = el.parent();

//         parent.css({
//             'background-image': 'url(' + encodeURI(src) + ')',
//             'background-size': 'cover',
//             'background-position': 'center',
//             'display': 'block',
//             'background-repeat': 'no-repeat'
//         });
//         console.log('"' + encodeURI(src) + '"');
//         el.hide();
//     });



// })(jQuery);