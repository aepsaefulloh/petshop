-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 20, 2022 at 04:22 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `namalengkap` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idadmin`, `email`, `username`, `password`, `namalengkap`) VALUES
(2, 'superadmin@gmail.com', 'admin', '1', 'Super Admin'),
(5, 'mamancandy@gmail', 'mamancuy', '1', 'Maman Candy');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Makanan Anjing'),
(2, 'Makanan Kucing'),
(3, 'Aksesoris'),
(5, 'Pasir'),
(6, 'Test1');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `idppelanggan` int(11) NOT NULL,
  `emailpelanggan` varchar(100) NOT NULL,
  `passwordpelanggan` varchar(50) NOT NULL,
  `namapelanggan` varchar(100) NOT NULL,
  `telppelanggan` varchar(25) NOT NULL,
  `alamatpelanggan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`idppelanggan`, `emailpelanggan`, `passwordpelanggan`, `namapelanggan`, `telppelanggan`, `alamatpelanggan`) VALUES
(5, 'abrhm@gmail.com', 'abraham', 'Abraham', '087271717273', 'Jakarta'),
(6, 'admin@admin.com', '1', 'Aep Saefulloh', '087873835503', 'Jl. Rengas 2 '),
(7, 'rafikadevilia@gmail.com', '1', 'Rafika Devilia', '087873835503', 'Jl. Bangka VIIIC');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `idpembayaran` int(11) NOT NULL,
  `idpembelian` int(11) NOT NULL,
  `token_bayar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `idpembelian` int(11) NOT NULL,
  `idppelanggan` int(11) NOT NULL,
  `no_order` varchar(255) DEFAULT NULL,
  `tanggalpembelian` date NOT NULL,
  `totalpembelian` int(11) NOT NULL,
  `namakota` varchar(100) NOT NULL,
  `kurir` varchar(50) NOT NULL,
  `tarif` int(11) NOT NULL,
  `alamatpengiriman` text NOT NULL,
  `postal_code` varchar(15) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`idpembelian`, `idppelanggan`, `no_order`, `tanggalpembelian`, `totalpembelian`, `namakota`, `kurir`, `tarif`, `alamatpengiriman`, `postal_code`, `status`) VALUES
(17, 5, 'PS1640166291', '2021-12-22', 165500, 'Jakarta Barat', 'jne REG', 16000, 'Jakarta', '54465', 1),
(18, 5, 'PS1640170334', '2021-12-22', 232000, 'Indramayu', 'jne REG', 22000, 'Jakarta', '45254', 1),
(24, 5, 'PS1640183228', '2021-12-22', 338000, 'Jakarta Barat', 'jne YES', 38000, 'Jakarta', '55565', 1),
(25, 5, 'PS1640235188', '2021-12-23', 301000, 'Jakarta Pusat', 'jne YES', 38000, 'Jakarta', '55445', 1),
(27, 5, 'PS1640237354', '2021-12-23', 332000, 'Jakarta Selatan', 'jne REG', 32000, 'Jakarta', '45334', 0),
(33, 5, 'PS1640801149', '2021-12-29', 226000, 'Jakarta Pusat', 'jne REG', 16000, 'Jakarta', '33544', 1),
(34, 5, 'PS1640851571', '2021-12-30', 142000, 'Indramayu', 'jne REG', 22000, 'Jakarta', '99877', 0),
(35, 5, 'PS1640851660', '2021-12-30', 550000, 'Bekasi', 'jne YES', 80000, 'Jakarta', '66767', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelianproduk`
--

CREATE TABLE `pembelianproduk` (
  `idpembelianproduk` int(11) NOT NULL,
  `idpembelian` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) DEFAULT NULL,
  `subberat` int(11) DEFAULT NULL,
  `subharga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelianproduk`
--

INSERT INTO `pembelianproduk` (`idpembelianproduk`, `idpembelian`, `idproduk`, `jumlah`, `nama`, `harga`, `berat`, `subberat`, `subharga`) VALUES
(14, 17, 28, 2, 'Royal Canin Persian Adult', 9500, 80, 160, 19000),
(16, 18, 32, 1, 'Royal Canin Chihuahua', 210000, 800, 800, 210000),
(26, 25, 29, 3, 'Royal Canin Hair and Skin Care', 75000, 55, 165, 225000),
(28, 27, 31, 2, 'Royal Canin Digestive Care', 150000, 1000, 2000, 300000),
(34, 33, 28, 3, 'Royal Canin Persian Adult', 70000, 80, 240, 210000),
(35, 34, 26, 1, 'Royal Canin Urinary Care Gravy', 120000, 85, 85, 120000),
(36, 35, 32, 2, 'Royal Canin Chihuahua', 50000, 800, 1600, 100000),
(37, 35, 33, 1, 'Bandana Anjing', 50000, 50, 50, 50000);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `idproduk` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `namaproduk` varchar(100) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `hargaproduk` int(11) NOT NULL,
  `beratproduk` int(11) NOT NULL,
  `fotoproduk` varchar(100) NOT NULL,
  `deskripsiproduk` text NOT NULL,
  `discount` varchar(50) DEFAULT NULL,
  `hit` int(11) DEFAULT '0',
  `terjual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`idproduk`, `id_kategori`, `namaproduk`, `stok`, `nama_kategori`, `hargaproduk`, `beratproduk`, `fotoproduk`, `deskripsiproduk`, `discount`, `hit`, `terjual`) VALUES
(20, 2, 'WhiskasÂ® Pouch Mackerel Senior 80g', 1, 'Makanan Kucing', 100000, 80, '123213.png', '			Pada usia 7 tahun, kucing Anda mungkin tidak terlihat atau bertindak jauh berbeda, tetapi kebutuhan nutrisi mereka telah berubah. Misalnya, sistem pencernaan mereka tidak sekuat dulu, sehingga kemampuan mereka untuk menyerap nutrisi melemah. Bantu mereka beralih ke WHISKAS Dewasa 7+, khusus diformulasikan untuk menunda efek usia tua.\r\n\r\n-Sepesifik diformulasikan untuk kucing usia 1 tahun keatas\r\n-Mengandung Omega 3 & 6 , serta Zinc untuk kesehatan kulit dan bulu yang indah.\r\n-Mengandung Vit.A dan taurin untuk kesehatan matanya\r\n-Mengandung protein dari ikan yang segar, lemak yang baik, vitamin dan mineral yang seimbang, untuk tetap menjaga kucingmu aktif dan bahagia.\r\n-Mengandung Antioksidan (Vit E dan Selenium )  untuk kesehatan sistem imunnya		', NULL, 1, 0),
(21, 2, 'WhiskasÂ® Dry Adult Dewasa 7+, Cat food Rasa Mackerel', 5, 'Makanan Kucing', 26000, 1000, '2343242342.png', '			rasa nya seperti ikan sapu sapu				', NULL, 0, 0),
(22, 2, 'WhiskasÂ® Pouch Tuna Senior 80g', 10, 'Makanan Kucing', 23000, 80, '999.png', '			-Sepesifik diformulasikan untuk kucing usia 1 tahun keatas\r\n-Mengandung Omega 3 & 6 , serta Zinc untuk kesehatan kulit dan bulu yang indah.\r\n-Mengandung Vit.A dan taurin untuk kesehatan matanya\r\n-Mengandung protein dari ikan yang segar, lemak yang baik, vitamin dan mineral yang seimbang, untuk tetap menjaga kucingmu aktif dan bahagia.\r\n-Mengandung Antioksidan (Vit E dan Selenium )  untuk kesehatan sistem imunnya		', NULL, 0, 0),
(23, 1, 'Royal Canin Mother Baby Cat', 12, 'Makanan Anjing', 34000, 1000, '345.png', 'mantap loh				', NULL, 0, 0),
(24, 1, 'Royal Canin Beagle Adult', 8, 'Makanan Anjing', 60000, 1000, 'ad-beagle-packshot-bhn18.png', 'Kering, Anjing\r\nComplete feed for dogs - Specially for adult and mature Beagles - Over 12 months old.\r\n\r\n', NULL, 0, 0),
(25, 1, 'Royal Canin Mini Adult 8+', 3, 'Makanan Anjing', 20000, 500, 'packshot-mini-ad8-shn17.png', 'enak', NULL, 4, 0),
(26, 2, 'Royal Canin Urinary Care Gravy', 5, 'Makanan Kucing', 120000, 85, 'fcnw16-urinary-cig-n-pouch.png', 'Complete feed for adult cats (thin slices in gravy).\r\n\r\n', NULL, 4, 1),
(27, 2, 'Royal Canin Indoor Long Hair', 8, 'Makanan Kucing', 68000, 80, '16-indoor-long-hair-b1-se.png', 'GB - Balanced and complete feed for cats - Specially for adult long-haired cats (from 1 to 7 years old) living indoors\r\n\r\n', NULL, 0, 0),
(28, 2, 'Royal Canin Persian Adult', 0, 'Makanan Kucing', 70000, 80, 'fbn2016-packshot-persian-adult.png', 'Balanced and complete feed for cats - Specially for adult Persian cats - Over 12 months old.', NULL, 113, 2),
(29, 2, 'Royal Canin Hair and Skin Care', 1, 'Makanan Kucing', 75000, 55, 'hairskin-ne-fcn-packshot.png', 'manis rasanya', NULL, 4, 0),
(30, 2, 'Royal Canin Inactive Jelly', 1, 'Makanan Kucing', 60000, 55, 'fhnw16-ins-cij-n-pouch.png', 'jelly', NULL, 7, 2),
(31, 1, 'Royal Canin Digestive Care', 0, 'Makanan Anjing', 150000, 1000, 'ccn18-digestive-facing-pouch-85-s.png', 'Complete feed for dogs - For adult dogs over 10 months old - Dogs prone to digestive sensitivity.', NULL, 47, 0),
(32, 1, 'Royal Canin Chihuahua', 10, 'Makanan Anjing', 210000, 800, 'breed-18-chihuahua-s-pouch-85.png', 'Chihuahua ', NULL, 19, 1),
(33, 3, 'Bandana Anjing', 11, 'Aksesoris', 50000, 50, 'bandana-anjing.png', 'Bandana Halloween', NULL, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `idrating` int(11) NOT NULL,
  `idpembelian` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `rating` enum('1','2','3','4','5') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`idrating`, `idpembelian`, `idproduk`, `rating`) VALUES
(6, 25, 29, '5'),
(7, 33, 28, '5'),
(9, 35, 32, '4'),
(10, 35, 33, '5');

-- --------------------------------------------------------

--
-- Table structure for table `retur`
--

CREATE TABLE `retur` (
  `id_retur` int(11) NOT NULL,
  `idpembelian` int(11) NOT NULL,
  `subjek` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur`
--

INSERT INTO `retur` (`id_retur`, `idpembelian`, `subjek`, `keterangan`, `foto`) VALUES
(2, 33, 'Pengembalian ', 'Karena bungkus rusak', 'retur-276205235.jpg'),
(14, 35, 'Pengembalian ', 'tes pengembalian', 'retur-76942382.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ulasan`
--

CREATE TABLE `ulasan` (
  `idulasan` int(11) NOT NULL,
  `idpembelian` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `teks_ulasan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ulasan`
--

INSERT INTO `ulasan` (`idulasan`, `idpembelian`, `idproduk`, `teks_ulasan`) VALUES
(6, 25, 29, 'Sangat cocok'),
(7, 33, 28, 'Ini sangat cocok buat kucing saya!!!'),
(9, 35, 32, 'mantap'),
(10, 35, 33, 'bagus');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`idppelanggan`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`idpembayaran`),
  ADD KEY `idpembelian` (`idpembelian`) USING BTREE;

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`idpembelian`),
  ADD KEY `idppelanggan` (`idppelanggan`);

--
-- Indexes for table `pembelianproduk`
--
ALTER TABLE `pembelianproduk`
  ADD PRIMARY KEY (`idpembelianproduk`),
  ADD KEY `idpembelian` (`idpembelian`),
  ADD KEY `idproduk` (`idproduk`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`idproduk`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`idrating`),
  ADD KEY `idpembelian` (`idpembelian`),
  ADD KEY `idproduk` (`idproduk`);

--
-- Indexes for table `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`id_retur`),
  ADD KEY `idpembelian` (`idpembelian`);

--
-- Indexes for table `ulasan`
--
ALTER TABLE `ulasan`
  ADD PRIMARY KEY (`idulasan`),
  ADD KEY `idpembelian` (`idpembelian`),
  ADD KEY `idproduk` (`idproduk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `idppelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `idpembayaran` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `idpembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `pembelianproduk`
--
ALTER TABLE `pembelianproduk`
  MODIFY `idpembelianproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `idproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `idrating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `retur`
--
ALTER TABLE `retur`
  MODIFY `id_retur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ulasan`
--
ALTER TABLE `ulasan`
  MODIFY `idulasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`idpembelian`) REFERENCES `pembelian` (`idpembelian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD CONSTRAINT `pembelian_ibfk_1` FOREIGN KEY (`idppelanggan`) REFERENCES `pelanggan` (`idppelanggan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembelianproduk`
--
ALTER TABLE `pembelianproduk`
  ADD CONSTRAINT `pembelianproduk_ibfk_1` FOREIGN KEY (`idpembelian`) REFERENCES `pembelian` (`idpembelian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pembelianproduk_ibfk_2` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`idpembelian`) REFERENCES `pembelian` (`idpembelian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_ibfk_2` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retur`
--
ALTER TABLE `retur`
  ADD CONSTRAINT `retur_ibfk_1` FOREIGN KEY (`idpembelian`) REFERENCES `pembelian` (`idpembelian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ulasan`
--
ALTER TABLE `ulasan`
  ADD CONSTRAINT `ulasan_ibfk_1` FOREIGN KEY (`idpembelian`) REFERENCES `pembelian` (`idpembelian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ulasan_ibfk_2` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
