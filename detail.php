<?php session_start();
$koneksi = new mysqli("localhost", "root", "", "petshop");
$idproduk = $_GET["id"];
$ambil = $koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
$detail = $ambil->fetch_assoc();

$query  = $koneksi->query("UPDATE produk SET hit = hit+1 WHERE idproduk='$_GET[id]'");
$query  = $koneksi->query("SELECT hit FROM produk WHERE idproduk='$_GET[id]'");
$CurrentHit = mysqli_fetch_array($query);
$hits = $CurrentHit['hit'];

// echo 'Total hits on page'.$hits.'';

?>
<!DOCTYPE html>
<?php
// print_r($UpdateField);

?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop - <?php echo $detail["namaproduk"]; ?></title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css" rel="stylesheet">
    </link>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand() ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand() ?>">



</head>

<body>
    <?php require_once 'include/header.php'; ?>
    <section class="section-timezone ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 align-self-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                            <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                            <li class="breadcrumb-item"><a href="/produk.php">Produk</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $detail["namaproduk"]; ?>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-4 text-center align-self-center">
                    <span><span id="city"></span> | <span id="date"></span> | <span id="time"></span></span>

                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </section>

    <section class="product-detail my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="feature-image">
                        <img id="img" src="fotoproduk/<?php echo $detail["fotoproduk"];  ?>" class="img-fluid">
                    </div>

                </div>
                <div class="col-md-5 offset-md-2">
                    <div class="product-name">
                        <h4>
                            <?php echo $detail["namaproduk"]; ?>
                        </h4>
                        <hr>
                    </div>
                    <form method="post">
                        <div class="store-heading">
                            <p>TERJUAL : <?= $detail['terjual']  ?></p>
                            <p>PRICE :</p>

                            <?php
                            if ($detail['discount'] != 0) {
                                echo '<del><small>Rp.' . number_format($detail['hargaproduk']) . '</small></del>';
                                echo '<h5 class="mb-3">Rp. ' . number_format(($detail["hargaproduk"]) - ($detail["hargaproduk"]) * $detail["discount"]) . '</h5>';
                            } else {
                                echo '<h5 class="mb-3">Rp.' . number_format($detail["hargaproduk"]) . '</h5>';
                            }
                            ?>
                            <p>QTY :</p>
                            <input type="number" name="jumlah" value="1" required min="1" 
                            <?php if ($detail['stok'] == 0) {
                                    echo 'readonly';
                                } else {
                                    echo 'max="' . $detail['stok'] . '"';
                                } ?> value=""
                                style="padding:5px;width:70px">
                            <small>Stok : <?php echo $detail['stok'] ?></small>

                        </div>
                        <?php
                        if (isset($_SESSION['pelanggan']) == 0) {
                        ?>

                        <a href="javascript:void(0)" class="btn btn-dark w-100 my-3" onclick="loginAlert()">Login</a>

                        <?php } else if ($detail['stok'] == 0) { ?>

                        <button class="btn btn-dark w-100 my-3" name="beli" disabled>Stok Habis</button>
                        <div class="alert alert-danger" role="alert">
                            “Hai Pembeli yang terhormat, berhubung banyaknya permintaan pesanan terhadap produk ini,
                            kami mohon maaf karena tidak dapat memproses pesanan Anda. Terima kasih telah antusias
                            dengan produk kami dan nantikan produk ini tersedia kembali”.
                        </div>
                        <?php } else { ?>

                        <button class="btn btn-dark w-100 my-3" name="beli">Tambah
                            Keranjang</button>

                        <?php } ?>

                    </form>
                    <?php
                    if (isset($_POST["beli"])) {
                        $jumlah = $_POST["jumlah"];


                        if (isset($_SESSION['keranjang'][$idproduk])) {
                            $_SESSION['keranjang'][$idproduk] += $jumlah;
                        } else {
                            $_SESSION['keranjang'][$idproduk] = $jumlah;
                        }

                        echo "<script>alert('Produk telah masuk ke keranjang');</script>";
                        echo "<script>location='detail.php?id=$idproduk';</script>";
                    }

                    ?>
                    <div class="info-product">
                        <h6 class="text-muted mt-3"><strong> INFO PRODUK :</strong></h6>
                        <div class="product-info">
                            <p class="text-muted">
                                <?php echo $detail["deskripsiproduk"]; ?>
                            </p>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h5>Rating:</h5>
                    <?php
                    $rating5 = $koneksi->query("SELECT rating FROM rating WHERE idproduk = $idproduk AND rating = 5")->num_rows;
                    $rating4 = $koneksi->query("SELECT rating FROM rating WHERE idproduk = $idproduk AND rating = 4")->num_rows;
                    $rating3 = $koneksi->query("SELECT rating FROM rating WHERE idproduk = $idproduk AND rating = 3")->num_rows;
                    $rating2 = $koneksi->query("SELECT rating FROM rating WHERE idproduk = $idproduk AND rating = 2")->num_rows;
                    $rating1 = $koneksi->query("SELECT rating FROM rating WHERE idproduk = $idproduk AND rating = 1")->num_rows;

                    include 'include/rating-produk.php';
                    ?>
                </div>
                <?php include 'include/star.php' ?>
                <div class="col-12 mt-5">
                    <h5>Ulasan:</h5>
                    <?php
                    $getUlasan = $koneksi->query("SELECT u.idpembelian,idproduk,teks_ulasan,namapelanggan FROM ulasan u 
                        JOIN pembelian p ON u.idpembelian = p.idpembelian JOIN pelanggan pl ON pl.idppelanggan = p.idppelanggan  
                        WHERE u.idproduk = $idproduk");

                    if ($getUlasan->num_rows == 0) {
                        echo '<div class="card"><div class="card-body text-center">Belum ada ulasan!</div></div>';
                    }
                    while ($ulasan = $getUlasan->fetch_assoc()) :
                        $getRating = $koneksi->query("SELECT rating FROM rating WHERE idpembelian = {$ulasan['idpembelian']} AND idproduk  = {$ulasan['idproduk']}")->fetch_assoc();
                    ?>
                    <div class="card mb-4">
                        <div class="card-header" style="display: flex;align-items: center;">
                            <?= $ulasan['namapelanggan'] ?>
                            <div class="ms-3">
                                <?= $star[$getRating['rating']] ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <?= $ulasan['teks_ulasan'] ?>
                        </div>
                    </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </section>

    </div>
    <?php require_once 'include/footer.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
    <script src="assets/js/app.js"></script>

    <script>
    function loginAlert() {
        alert("Login Terlebih dahulu untuk melanjutkan transaksi");
        window.location.href = 'login.php';
    };
    $("[type='number']").keypress(function(evt) {
        evt.preventDefault();
    });
    </script>


</body>

</html>