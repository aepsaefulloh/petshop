<?php
session_start();

$koneksi = new mysqli("localhost", "root", "", "petshop");
if (!isset($_SESSION['pelanggan'])) {
  header('location: login.php');
}

if (!isset($_GET['id']) || empty($_GET['id'])) {
  header('location: riwayat.php');
}

$idpembelian = $_GET['id'];
$query = $koneksi->query("SELECT * FROM pembelian WHERE idpembelian = {$idpembelian}");

if ($query->num_rows == 0) {
  header('location: riwayat.php');
}
$datapembelian = $query->fetch_assoc();

if (isset($_POST['submit'])) {
  if (empty($_POST['rating']) || empty($_POST['teks_ulasan'])) {
    echo '<script>alert("Isi semua form dengan benar!")</script>';
  } else {
    $rating = $_POST['rating'];
    $teks_ulasan = $_POST['teks_ulasan'];
    $idproduk = $_POST['idproduk'];

    $koneksi->query("INSERT INTO rating VALUES('', $idpembelian, $idproduk, $rating)");
    $koneksi->query("INSERT INTO ulasan VALUES('', $idpembelian, $idproduk, '$teks_ulasan')");

    echo '<script>alert("Berhasil memberikan rating dan ulasan!")</script>';
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Palagan Petshop</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/star-rating.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand() ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand() ?>">

</head>

<body>
  <?php require_once 'include/header.php'; ?>

  <section class="section-cart my-5">
    <div class="container" style="width: 560px;">
      <div class="card">
        <div class="card-header text-center">
          <h5>Ulasan Produk</h5>
        </div>
        <div class="card-body">
          <table class="table">
            <?php
            $getproduk = $koneksi->query("SELECT * FROM pembelianproduk WHERE idpembelian = $idpembelian");

            while ($produk = $getproduk->fetch_assoc()) :
              $p = $koneksi->query("SELECT fotoproduk FROM produk WHERE idproduk = {$produk['idproduk']}")->fetch_assoc();
              $getRating = $koneksi->query("SELECT * FROM rating WHERE idproduk = {$produk['idproduk']} AND idpembelian = {$idpembelian}");
              $getUlasan = $koneksi->query("SELECT * FROM ulasan WHERE idproduk = {$produk['idproduk']} AND idpembelian = {$idpembelian}");

              $rating = $getRating->fetch_array();
              $ulasan = $getUlasan->fetch_array();
            ?>
              <tr>
                <td style="border-style: none;"><img src="fotoproduk/<?= $p['fotoproduk'] ?>" style="height: 150px"></td>
                <td style="border-style: none;">
                  <h5><?= $produk['nama'] ?></h5>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <form action="" method="POST">
                    <input type="hidden" name="idproduk" value="<?= $produk['idproduk'] ?>">
                    <div class="mb-3">
                      <label class="form-label">Rating:</label>
                      <select class="star-rating" name="rating" <?= $getRating->num_rows == 0 ? 'required' : 'disabled' ?>>
                        <option value="">Select a rating</option>
                        <option value="5" <?= $rating && $rating['rating'] == 5 ? 'selected' : '' ?>>Excellent</option>
                        <option value="4" <?= $rating && $rating['rating'] == 4 ? 'selected' : '' ?>>Very Good</option>
                        <option value="3" <?= $rating && $rating['rating'] == 3 ? 'selected' : '' ?>>Average</option>
                        <option value="2" <?= $rating && $rating['rating'] == 2 ? 'selected' : '' ?>>Poor</option>
                        <option value="1" <?= $rating && $rating['rating'] == 1 ? 'selected' : '' ?>>Terrible</option>
                      </select>
                    </div>
                    <div class="mb-3">
                      <label class="form-label"><b>Ulasan:</b></label>
                      <textarea class="form-control" name="teks_ulasan" rows="3" placeholder="Tulisankan ulasan anda" <?= $getRating->num_rows == 0 ? 'required' : 'disabled' ?>><?= $ulasan ? $ulasan['teks_ulasan'] : '' ?></textarea>
                    </div>
                    <?php if ($getRating->num_rows == 0 && $getUlasan->num_rows == 0): ?>
                    <div class="mb-3 text-end">
                      <button type="submit" name="submit" class="btn btn-success w-25">Kirim</button>
                    </div>
                    <?php endif; ?>
                  </form>
                </td>
              </tr>
            <?php
            endwhile;
            ?>
          </table>
        </div>
      </div>
    </div>
  </section>

  <?php require_once 'include/footer.php'; ?>

  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <script src="assets/js/star-rating.js"></script>
  <script>
    var starRatingControl = new StarRating('.star-rating', {
      maxStars: 5,
      tooltip: false
    });
  </script>
</body>

</html>