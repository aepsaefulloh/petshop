<?php 
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop - Makanan Anjing</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">



</head>

<body>
    <?php require_once 'include/header.php'; ?>
    <section class="section-timezone ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 align-self-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Makanan Anjing
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-4 text-center align-self-center">
                    <span>Jogja | <span id="date"></span> | <span id="time"></span></span>

                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </section>
    <!--konten-->
    <section class="section-content my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h4>Makanan Anjing</h4>
                    </div>
                    <div class="grid-product">
                        <?php 
                           	$limit = 12;
                            $id_kategori = 1;
                            $orderBy="WHERE id_kategori = '$id_kategori' GROUP BY idproduk desc";
                            $halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
                            $halaman_awal = ($halaman>1) ? ($halaman * $limit) - $limit : 0;	
                            $previous = $halaman - 1;
                            $next = $halaman + 1;
                            $data = mysqli_query($koneksi,"SELECT count(*) as total from produk");
                            $jumlah_data = mysqli_num_rows($data);
                            $total_halaman = ceil($jumlah_data / $limit);
                            $getProduct = mysqli_query($koneksi,"select * from produk $orderBy limit $limit");
                            while($item = mysqli_fetch_array($getProduct)){
                            ?>
                        <figure class="figure">
                            <?php
                                if($item['discount'] != null){
                                    echo '<span class="btn btn-sm btn-danger m-2 rounded">'.($item['discount'] * 100).'%</span>';
                                }else{
                                    echo '';
                                }
                            ?>
                            <a href="detail.php?id=<?php echo $item["idproduk"]; ?>">
                                <img src="fotoproduk/<?php echo $item['fotoproduk'] ?>"
                                    class="figure-img img-fluid rounded" alt="...">
                                <figcaption class="figure-caption ps-3">
                                    <h3>
                                        <?php echo $item['namaproduk'] ?>
                                    </h3>
                                    <!-- <p>Rp.<?php echo number_format($item['hargaproduk']) ?></p> -->
                                    <?php
                                            if( $item['discount'] != null){
                                                echo '<del><small>Rp.'. number_format($item['hargaproduk']).'</small></del>';
                                                echo '<p>Rp. '.number_format(($item["hargaproduk"]) - ($item["hargaproduk"]) * $item["discount"]).'</p>';
                                            }else{
                                                echo '<p>Rp. '.number_format($item["hargaproduk"]).'</p>';
                                            }
                                        ?>
                                </figcaption>
                            </a>
                        </figure>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                                <a class="page-link"
                                    <?php if($halaman > 1){ echo "href='?halaman=$previous'"; } ?>>Previous</a>
                            </li>
                            <?php 
                                for($x=1;$x<=$total_halaman;$x++){
                                    //style
                                    $st='';
                                    if($halaman==$x) $st='background:#ccc';		
                            ?>
                            <li class="page-item"><a class="page-link" style='<?php echo $st?>'
                                    href="?halaman=<?php echo $x ?>"><?php echo $x; ?></a></li>
                            <?php
				                      }
				                    ?>
                            <li class="page-item">
                                <a class="page-link"
                                    <?php if($halaman < $total_halaman) { echo "href='?halaman=$next'"; } ?>>Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <style>

    </style>
    <?php require_once 'include/footer.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/app.js?<?php echo rand()?>"></script>
</body>

</html>