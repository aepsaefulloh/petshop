<?php 
session_start();
/*echo "<pre>";
print_r($_SESSION['keranjang']);
echo "</pre>";*/

$koneksi = new mysqli("localhost","root","","petshop");

if (empty($_SESSION["keranjang"]) OR !isset($_SESSION["keranjang"])) 
{
	# code...
	// echo "<script>alert('Keranjang kosong silahkan belanja dulu!');</script>";
	// echo "<script>location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Keranjang Belanja</title>
    <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<style>
body {
    background-image: url(hu.jpg);
    background-size: cover;
    background-attachment: fixed;
}
</style>

<body>


    <?php include 'menu.php'; ?>
    <section class="konten">
        <div class="container">
            <h1>Keranjang Belanja</h1>
            <hr>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>Harga Produk</th>
                        <th>Jumlah Yang Dibeli</th>
                        <th>Sub Harga</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
					$nomor = 1;
				    ?>
                    <?php foreach ($_SESSION ["keranjang"] as $idproduk => $jumlah): ?>
                    <?php 
					$ambil = $koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
					$pecah = $ambil->fetch_assoc();
					$subharga = $pecah["hargaproduk"]*$jumlah;
					?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah["namaproduk"]; ?></td>
                        <td>Rp. <?php echo number_format($pecah["hargaproduk"]); ?></td>
                        <td><?php echo $jumlah; ?></td>
                        <td>Rp. <?php echo number_format($subharga); ?></td>
                        <td>
                            <a href="hapuskeranjang.php?id=<?php echo $idproduk ?>"
                                class="btn btn-danger btn-xs">Hapus</a>
                        </td>
                    </tr>
                    <?php
					$nomor++
				?>
                    <?php endforeach ?>
                </tbody>
            </table>
            <a href="index.php" class="btn btn-default">Lanjutkan Belanja</a>
            <a href="checkout-old.php" class="btn btn-primary">Checkout</a>
        </div>
    </section>
</body>

</html>