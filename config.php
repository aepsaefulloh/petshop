<?php 
date_default_timezone_set("Asia/Bangkok");
define('DB_HOST', 'localhost');
define('DB_UID', 'root');
define('DB_PWD', '');
define('DB_DATABASE', 'petshop');
define( "ROOT_PATH", "C:/laragon/www/project/petshop" );
define( "ROOT_URL", "localhost/petshop");

define( "CACHE_URL", ROOT_URL."/cache" );
define( "CACHE_PATH", ROOT_PATH."/cache" );

?>