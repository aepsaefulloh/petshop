<?php
//Koneksi database
// $koneksi = new mysqli("localhost","root","","petshop");
session_start();
include './ro_request.php';
// $myArray = array();
// if ($result = mysqli_query($koneksi, "SELECT ID, KAB FROM kab WHERE PROV_ID = ".$_GET['prov'])) {
//     while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
//         $myArray[] = $row;
//     }
//     mysqli_close($koneksi);
//     echo json_encode($myArray);
// }
if (
    $_SERVER['REQUEST_METHOD'] === 'GET' &&
    isset($_GET['city']) &&
    !empty($_GET['city'])
) {
  $requestOngkirJne = get_ongkir($_GET['city'], $_GET['total_berat'], 'jne');
  $requestOngkirPos = get_ongkir($_GET['city'], $_GET['total_berat'], 'pos');

  $ongkirSemua = array_merge($requestOngkirJne, $requestOngkirPos);

  echo json_encode($ongkirSemua);
}
