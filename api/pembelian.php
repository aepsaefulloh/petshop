<?php
$koneksi = new mysqli("localhost", "root", "", "petshop");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $json = file_get_contents('php://input');
  $data = json_decode($json);

  $cekPembayaran = $koneksi->query("SELECT * FROM pembayaran WHERE idpembelian = '{$data->id_pembelian}'");

  if (
    $data->status_code == 409 ||
    $data->status_code == 200
  ) {
    $tanggal = date("Y-m-d");
    $updateStatus = $koneksi->query("UPDATE pembelian SET status = 1
      WHERE idpembelian = {$data->id_pembelian}");
    $updateTanggalBayar = $koneksi->query("UPDATE pembayaran SET tanggal = '{$tanggal}'
      WHERE idpembelian = {$data->id_pembelian}");
  } else if (
    $data->status_code == 407
  ) {
    $updateStatus = $koneksi->query("UPDATE pembelian SET status = 2 
      WHERE idpembelian = {$data->id_pembelian}");
  }

  if ($cekPembayaran->num_rows == 0) {
    $pembayaran = $koneksi->query("INSERT INTO pembayaran VALUES(
      '', '{$data->id_pembelian}', '{$data->snapToken}', '{$data->bank}', NULL
    )");
  }

  echo json_encode(["status" => 'success']);
}