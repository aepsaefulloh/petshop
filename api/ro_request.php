<?php
$rajaOngkirKey = 'f529b2b2019673391bba45dd965ded4c';
$kodeKotaAsal = '501'; // yogyakarta

function ro_request($url, $method)
{
  global $rajaOngkirKey;
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "$url",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "$method",
    CURLOPT_HTTPHEADER => array(
      "key: $rajaOngkirKey"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    var_dump($err);
    die();
  } else {
    $decode = json_decode($response, true);

    return $decode['rajaongkir']['results'];
  }
}

function get_ongkir($kodeTujuan, $weight, $kurir)
{
  global $rajaOngkirKey;
  global $kodeKotaAsal;
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "origin=$kodeKotaAsal&destination=$kodeTujuan&weight=$weight&courier=$kurir",
    CURLOPT_HTTPHEADER => array(
      "key: $rajaOngkirKey"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    var_dump($err);
    die();
  } else {
    $decode = json_decode($response, true);

    return $decode['rajaongkir']['results'];
  }
}