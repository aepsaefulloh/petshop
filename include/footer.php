<footer>
    <div class="container-fluid">
        <div class="footer-right">
            <a href="" target="_blank"><i class="bi bi-instagram"></i></a>
            <a href="" target="_blank"><i class="bi bi-facebook"></i></a>
        </div>
        <div class="footer-left">
            <div class="footer-links">
                Copyrights © 2021 All Rights Reserved by Palagan Petshop.
            </div>
            <p>Palagan Petshop adalah bisnis ritel yang menjual berbagai jenis hewan. Palagan Petshop juga menjual
                makanan hewan, persediaan, dan aksesoris.</p>
        </div>
    </div>
</footer>