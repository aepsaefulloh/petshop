<div class="row justify-content-between">
  <div class="col">
    <div class="card">
      <div class="card-body text-center">
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <p class="text-center p-0 mb-0 mt-2">(<?= $rating5 ?>)</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <div class="card-body text-center">
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <p class="text-center p-0 mb-0 mt-2">(<?= $rating4 ?>)</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <div class="card-body text-center">
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <p class="text-center p-0 mb-0 mt-2">(<?= $rating3 ?>)</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <div class="card-body text-center">
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <p class="text-center p-0 mb-0 mt-2">(<?= $rating2 ?>)</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <div class="card-body text-center">
        <img src="assets/img/star-full.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <img src="assets/img/star-empty.svg" width="15px" />
        <p class="text-center p-0 mb-0 mt-2">(<?= $rating1 ?>)</p>
      </div>
    </div>
  </div>
</div>