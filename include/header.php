<!-- Navbar -->
<nav class="navbar navbar-expand-sm  sticky-top navbar-light bg-light main-nav">
    <div class="container-fluid justify-content-center d-none d-md-flex">
        <ul class="nav navbar-nav w-100 flex-nowrap">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="produk.php">Produk</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    Kategori
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php 
                    $ambil = $koneksi->query("SELECT * FROM kategori ORDER BY id_kategori asc");
                    
                    while ($item = $ambil->fetch_assoc()) {
                        $url = 'kategori.php?id='.$item['id_kategori'];
                    ?>
                    <li><a class="dropdown-item" href="<?php echo $url ?>">
                            <?php echo $item['nama_kategori'] ?></a></li>
                    <?php } ?>
                    <!-- <li><a class="dropdown-item" href="pakankucing.php">Makanan Kucing</a></li>
                    <li><a class="dropdown-item" href="aksesoris.php">Aksesoris</a></li>
                    <li><a class="dropdown-item" href="pasir.php">Pasir</a></li> -->
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link text-center" href="index.php">
                    <img src="assets/img/logo/logo.png" class="main-logo" alt="main-logo">
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav w-100 justify-content-end">
            <?php 
                if(!isset($_SESSION['pelanggan'])){
            ?>
            <li class="nav-item active">
                <a class="nav-link" href="login.php">Masuk</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="daftar.php">Daftar</a>
            </li>
            <?php }else{ ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    <i class="bi bi-person-circle"></i>&nbsp;<?php echo $_SESSION['pelanggan']['namapelanggan'] ?>
                </a>

                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item" href="profile.php">Profile</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="riwayat.php">Riwayat Pembelian</a>
                    </li>
                    <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                </ul>
            </li>

            <li class="nav-link">
                <a href="keranjang.php">
                    <div class="add-cart">
                        <img src="assets/img/logo/cart.png" class="icon-cart" alt="cart">
                        <?php 
                            if(!empty($_SESSION["keranjang"])){
                        ?>
                        <span class="badge bg-danger">
                            <?php echo count(is_countable($_SESSION['keranjang'])?$_SESSION['keranjang']:[]) ?>
                        </span>
                        <?php }else{ ?>
                        <span class="badge bg-danger d-none">

                        </span>
                        <?php } ?>
                    </div>
                </a>
            </li>
            <?php }?>
        </ul>
    </div>
    <div class="container-fluid justify-content-between d-flex d-md-none">
        <ul class="nav navbar-nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="index.php">
                    <img src="assets/img/logo/logo.png?<?php echo rand()?>" class="main-logo" alt="main-logo">
                </a>
            </li>
        </ul>
        <div class="add-cart" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight"
            aria-controls="offcanvasRight">
            <svg viewBox="0 0 150 50" width="40" height="40" fill="#000" fill-opacity=".6">
                <rect width="50" height="10"></rect>
                <rect y="25" width="100" height="10"></rect>
                <rect y="50" width="70" height="10"></rect>
            </svg>
        </div>
    </div>


</nav>
<!-- Navbar-End -->

<div class="offcanvas offcanvas-end offcanvas-cart" tabindex="-1" id="offcanvasRight"
    aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <a class="nav-link nav-offcanvas" href="">
            <img src="assets/img/logo/logo.png?<?php echo rand()?>" class="offcanvas-logo" alt="offcanvas-logo">
        </a>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <ul class="nav navbar-nav w-100 flex-nowrap">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    Kategori
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="pakananjing.php">Makanan Anjing</a></li>
                    <li><a class="dropdown-item" href="pakankucing.php">Makanan Kucing</a></li>
                    <li><a class="dropdown-item" href="aksesoris.php">Aksesoris</a></li>
                    <li><a class="dropdown-item" href="pasir.php">Pasir</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="produk.php">Produk</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="upload.php">Upload Pembayaran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php">Produk</a>
            </li>

        </ul>
    </div>
</div>