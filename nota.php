<?php
$koneksi = new mysqli("localhost","root","","petshop");
?>
<!DOCTYPE html>
<html>

<head>
    <title>Nota Pembelian</title>
    <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>

<body>
    <?php include 'menu.php'; ?>
    <section class="konten">
        <div class="container">
            <H1 align="center">Detail Pembelian</H1>

            <?php
					$ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pembelian.idppelanggan=pelanggan.idppelanggan WHERE pembelian.idpembelian='$_GET[id]'");
					$detail = $ambil->fetch_assoc();
				?>
            <div class="row">
                <div class="col-md-4">
                    <h3>Pembelian</h3>
                    <strong>No. pembelian: <?php echo $detail['idpembelian']; ?></strong><br>
                    Tanggal : <?php echo $detail['tanggalpembelian']; ?><br>
                    Total : Rp. <?php echo number_format($detail['totalpembelian']); ?>
                </div>
                <div class="col-md-4">
                    <h3>Pelanggan</h3>
                    <strong><?php echo $detail['namapelanggan']; ?></strong><br>
                    <p>
                        <?php echo $detail['telppelanggan']; ?><br>
                        <?php echo $detail['emailpelanggan']; ?>
                    </p>
                </div>
                <div class="col-md-4">
                    <h3>Pengiriman</h3>
                    <strong><?php echo $detail['namakota']; ?></strong><br>
                    Ongkos Kirim : Rp. <?php echo number_format($detail['tarif']); ?><br>
                    Alamat : <?php echo $detail['alamatpengiriman']; ?>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
					$nomor = 1;
				?>
                    <?php 
					$ambil = $koneksi->query("SELECT * FROM pembelianproduk WHERE idpembelian = '$_GET[id]'");
				?>
                    <?php
					while ($pecah = $ambil->fetch_assoc()) {
					# code...
				?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $pecah['nama']; ?></td>
                        <td>Rp. <?php echo number_format($pecah['harga']); ?></td>
                        <td><?php echo $pecah['jumlah']; ?></td>
                        <td>Rp. <?php echo number_format($pecah['subharga']); ?></td>
                    </tr>
                    <?php
					$nomor++;
				?>
                    <?php
					}
				?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-7">
                    <div class="alert alert-info">
                        <p> Silahkan melakukan pembayaran Rp. <?php echo number_format($detail['totalpembelian']); ?> ke
                            <br>
                            <strong>BANK BRI 5551-01-018590-53-6 A/N Palagan Petshop</strong>
                        </p>
                    </div>
                    <a href="index.php" class="btn btn-primary">Kembali ke menu utama</a>
                </div>
            </div>
        </div>
    </section>
</body>

</html>