<?php
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Pelanggan</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">
</head>
<style>
html,
body {
    height: 100%;
}
</style>

<body class="body-login d-flex justify-content-center align-items-center">
    <div class="section-login ">

        <div class="login-page">
            <div class="login-logo text-center">
                <img src="assets/img/logo/logo.png" class="img-fluid w-25" alt=""
                    onclick="window.location.href='index.php';" style="cursor:pointer;">
            </div>
            <div class="form">
                <form class="login-form" method="post">
                    <input type="text" name="email" placeholder="email" required />
                    <input type="password" name="password" placeholder="password" required />
                    <button name="login">Masuk</button>
                    <p class="message">Belum Registrasi? <a href="daftar.php">Bikin Akun</a></p>
                </form>
            </div>
        </div>


    </div>

    <?php
	if (isset($_POST["login"])) 
	{
		# code...
		$email = $_POST["email"];
		$password = $_POST["password"];

		$ambil = $koneksi->query("SELECT * FROM pelanggan WHERE emailpelanggan = '$email' AND passwordpelanggan = '$password'"); 
		$balance = $ambil->num_rows;
		if ($balance==1) 
		{
			$akun = $ambil->fetch_assoc();
			$_SESSION["pelanggan"] = $akun;
			echo "<script>alert('Anda sukses login');</script>";
			echo "<script>location='index.php';</script>";
		}
		else
		{
			echo "<script>alert('Anda gagal login');</script>";
			echo "<script>location='login.php';</script>";
		}
	}

?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    </script>
</body>

</html>