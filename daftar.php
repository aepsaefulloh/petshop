<?php
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Pelanggan</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">
</head>
<style>
html,
body {
    height: 100%;
}
</style>

<body class="body-login d-flex justify-content-center align-items-center">
    <div class="section-login ">

        <div class="login-page">
            <div class="login-logo text-center">
                <img src="assets/img/logo/logo.png" class="img-fluid w-25" alt=""
                    onclick="window.location.href='index.php';" style="cursor:pointer;">
            </div>
            <div class="form">
                <form class="login-form" method="post">
                    <input type="text" name="nama" placeholder="Nama Lengkap" required />
                    <input type="text" name="email" placeholder="Email" required />
                    <input type="password" name="password" placeholder="password" required />
                    <input type="text" name="alamat" placeholder="Alamat" required />
                    <input type="number" name="telepon" placeholder="Nomor telepon" required />
                    <button name="daftar">Daftar</button>
                    <p class="message">Sudah memiliki akun? <a href="login.php">Masuk</a></p>
                </form>
            </div>
        </div>


    </div>

    <?php
	if (isset($_POST['daftar'])) 
		{
			$nama = $_POST["nama"];
			$email = $_POST["email"];
			$password = $_POST["password"];
			$alamat = $_POST["alamat"];
			$telepon = $_POST["telepon"];

			$ambil = $koneksi->query("SELECT * FROM pelanggan WHERE emailpelanggan = '$email'");
			$balance = $ambil->num_rows;
			if ($balance==1) 
			{
				# code...
				echo "<script>alert('Pendaftaran gagal, Email sudah digunakan');</script>";
				echo "<script>location='daftar.php';</script>";
			}
			else
			{
				$koneksi->query("INSERT INTO pelanggan (emailpelanggan, passwordpelanggan, namapelanggan, telppelanggan, alamatpelanggan) VALUES
                 ('$email','$password','$nama','$telepon','$alamat')");

				echo "<script>alert('Pendaftaran Sukses, Silahkan Login');</script>";
				echo "<script>location='login.php';</script>";
			}
		}

?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    </script>
</body>

</html>