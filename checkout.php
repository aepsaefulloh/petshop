<?php 
session_start();
include 'api/ro_request.php';
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");

if (!isset($_SESSION["pelanggan"])) 
{
	# code...
	echo "<script>alert('Silahkan login');</script>";
	echo "<script>location='login.php';</script>";
}
if (!isset($_SESSION['checkout'])) {
    echo "<script>alert('Silahkan pilih barang dikeranjang dulu!');</script>";
    echo "<script>location='keranjang.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop - Checkout</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">



</head>

<body>
    <?php require_once 'include/header.php'; ?>

    <!-- Checkout Item -->
    <section class="section-checkout-item pt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col" class="text-center">Harga</th>
                                <th scope="col" class="text-center">Kuantitas</th>
                                <th scope="col" class="text-center">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $totalbelanja=0;
                            $totalBerat = 0;
                            foreach ($_SESSION["checkout"] as $idproduk => $jumlah){
                            $ambil = $koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
                            $pecah = $ambil->fetch_assoc();
                            if($pecah['discount'] !== 0 || $pecah['discount'] !== null){
                                $discount = $pecah["hargaproduk"] - $pecah["hargaproduk"] * $pecah["discount"];
                            }else{
                                $discount = number_format($pecah["hargaproduk"]);
                            }
                            $totalBerat += ($pecah['beratproduk'] * $jumlah);
                            $subharga = $discount*$jumlah;
                            $totalbelanja+=$subharga;
					    ?>
                            <tr>
                                <td class="td-img">
                                    <div class="checkout-item">
                                        <img src="fotoproduk/<?php echo $pecah["fotoproduk"];?>" class="item-image"
                                            alt="">
                                        <div class="checkout-info">
                                            <p class="checkout-name"><?php echo $pecah["namaproduk"]; ?></p>
                                            <p>Rp. <?php echo number_format($discount); ?> x
                                                <?php echo $jumlah; ?></p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">Rp. <?php echo number_format($discount); ?></td>
                                <td class="text-center"><?php echo $jumlah; ?></td>
                                <td class="text-center">Rp. <?php echo number_format($subharga); ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td class="text-center">
                                    TOTAL ITEM :
                                </td>
                                <td class="text-center">
                                    Rp. <?php echo number_format($totalbelanja); ?>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="section-profile py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="mb-3">
                                    <label class="form-label">Nama Lengkap</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $_SESSION["pelanggan"]["namapelanggan"] ?>" readonly>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Alamat Lengkap</label>
                                    <input type="text" class="form-control" name="alamatpengiriman"
                                        value="<?php echo $_SESSION["pelanggan"]["alamatpelanggan"] ?>" readonly>
                                    <div class="form-text">(Nama Jalan, No rumah, RT/RW dan patokan)</div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">No. Telepon</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $_SESSION["pelanggan"]["telppelanggan"] ?>" readonly>
                                </div>

                            </div>
                            <div class="col-md-5 offset-md-1">
                                <div class="mb-3">
                                    <?php
                                        $data_provinsi = ro_request('https://api.rajaongkir.com/starter/province', 'GET');
                                    ?>
                                    <label class="form-label" for="validationCustom04">Provinsi</label>
                                    <select class="form-select" id="province" name="province" required>
                                        <option selected disabled>---</option>
                                        <?php
                                            foreach ($data_provinsi as $prov) {
                                                echo '<option value="' . $prov['province_id'] . '-' . $prov['province'] . '">' . $prov['province'] . '</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Kota/Kabupaten</label>
                                    <select class="form-select" name="kab" id="kab" required>
                                        <option value="">-- Pilih Provinsi Terlebih Dahulu --</option>
                                    </select>
                                </div>

                                <div class="mb-3">
                                    <label class="form-label">Kode pos</label>
                                    <input type="text" class="form-control" required name="postal_code" placeholder="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Kurir</label>
                                    <select class="form-select" id="kurir" name="ongkir">
                                        <option selected disabled>-- Pilih Kota Terlebih Dahulu --</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="order-detail py-3">
                                <div class="mb-3">
                                    <label class="form-label">Order Detail</label>
                                    <div class="d-flex justify-content-between order-result">
                                        <span>Item Total</span>

                                        <span id="rp_count_price">Rp. <?php echo number_format($totalbelanja);?></span>
                                    </div>
                                    <div class="d-flex justify-content-between order-result">
                                        <span>Shipping</span>
                                        <span id="rp_shipping">Rp. 0</span>
                                    </div>
                                    <div class="d-flex justify-content-between order-result">
                                        <span>Total</span>
                                        <span id="total_payment">Rp. <?php echo number_format($totalbelanja)?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-dark mt-2" name="checkout">Submit</button>
                            </div>
                        </div>
                    </form>
                    <?php
			if (isset($_POST["checkout"])) 
			{
                // cek stok produk sebelum cekout
                $stokHabis = 0;
                foreach ($_SESSION['checkout'] as $id_produk => $jumlah) {
                    $cekStok = $koneksi->query("SELECT namaproduk,stok FROM produk WHERE idproduk = {$id_produk}")->fetch_assoc();

                    if ($jumlah > $cekStok['stok']) {
                        $adaStokHabis++;
                        echo "<script>alert('Stok untuk " . $cekStok['namaproduk'] . " tersisa " . $cekStok['stok'] . " !');</script>";
                        echo "<script>location='keranjang.php';</script>";
                    }
                }
                if ($adaStokHabis > 0) {
                    return;
                }

				# code...
				$idppelanggan = $_SESSION["pelanggan"]["idppelanggan"];
				$ongkir = explode("-", $_POST["ongkir"]);
				$tanggalpembelian = date("Y-m-d");
				$alamatpengiriman=$_POST['alamatpengiriman'];

				// $ambil = $koneksi->query("SELECT * FROM ongkir WHERE id_ongkir = '$id_ongkir'");
				// $arrayongkir = $ambil->fetch_assoc();
				$namakota = explode("-",$_POST['kab'])[1];   
				$tarif = $ongkir[1];
                $kurir = $ongkir[0];
                $no_order = 'PS'.time();
                // $prov_id = $_POST['province'];
                // $kab_id = $_POST['kab'];
                $postal_code = $_POST['postal_code'];

				$totalpembelian = $totalbelanja + $tarif;
				$koneksi->query("INSERT INTO pembelian VALUES 
                    ('', '$idppelanggan','$no_order','$tanggalpembelian','$totalpembelian','$namakota','$kurir','$tarif','$alamatpengiriman','$postal_code',0)");

				$idpembelibarusan = $koneksi->insert_id;
                
				foreach ($_SESSION["checkout"] as $idproduk => $jumlah) 
				{
					$ambil=$koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
					$perproduk = $ambil->fetch_assoc();
                    if($pecah['discount'] !== 0 || $pecah['discount'] !== null){
                        $discount = $pecah["hargaproduk"] - $pecah["hargaproduk"] * $pecah["discount"];
                    }else{
                        $discount = number_format($pecah["hargaproduk"]);
                    }
					$nama=$perproduk['namaproduk'];
					$harga=$discount;
					$berat=$perproduk['beratproduk'];
                    $stok=$pecah['stok'];

					$subberat=$perproduk['beratproduk']*$jumlah;
					$subharga=$discount*$jumlah;
                    $terjual=$perproduk['terjual'] + 1;
					$koneksi->query("INSERT INTO pembelianproduk (idpembelian,idproduk,jumlah,nama,harga,berat,subberat,subharga) VALUES 
                    ('$idpembelibarusan','$idproduk','$jumlah','$nama','$harga','$berat','$subberat','$subharga')");
                    $koneksi->query("UPDATE produk SET stok = $stok - $jumlah, terjual = $terjual WHERE idproduk='$idproduk'");
                    // echo print_r($koneksi);
                }

                foreach ($_SESSION['checkout'] as $key => $value) {
                    unset($_SESSION['keranjang'][$key]);
                }
				unset($_SESSION["checkout"]);

				echo "<script>alert('Pembelian sukses');</script>";
				echo "<script>location='order-success.php?id=$idpembelibarusan';</script>";
			}
			?>

                </div>
            </div>

        </div>
    </section>





    <?php require_once 'include/footer.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <script>
    // Onchange Kab
    $('#province').change(function() {
        let prov_id = $('#province').val();
        $('#kab').html('');
        $('#kab').append(`<option value="">Loading...</option>`);
        
        $.ajax({
            type: 'GET',
            url: "./api/kab.php?province=" + prov_id.split("-")[0],
            dataType: 'json',
            success: function(data) {
                $('#kab').html('');
                $('#kab').append(`<option value="">-- Pilih Kota/Kab --</option>`);

                data.forEach(x => {
                    $('#kab').append(`
                        <option value="${x.city_id}-${x.city_name}">${x.type} ${x.city_name}</option>
                    `);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            },
        });
    })

    $('#kab').change(function() {
        let kota = $("#kab").val();
        $('#kurir').html('');
        $('#kurir').append(`<option value="">Loading...</option>`);

        $.ajax({
            type: 'GET',
            url: `./api/ongkir.php?city=${kota.split("-")[0]}&total_berat=<?= $totalBerat ?>`,
            dataType: 'json',
            success: function(data) {
                $('#kurir').html('');
                $('#kurir').append(`<option value=""> -- Pilih kurir -- </option>`);

                data.forEach(x => {
                    x.costs.forEach(y => {
                        $('#kurir').append(`
                            <option value="${x.code} ${y.service}-${y.cost[0].value}">
                                ${x.code.toUpperCase()} | ${y.service} | Rp.${y.cost[0].value}
                            </option>
                        `);
                    })
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            },
        });
    })

    // Onchange Prices
    $('#kurir').change(function() {
        let count_price = $('#rp_count_price').text().replace(/\D/g, '');
        var value_shipping = ($('#kurir option:selected').text().split(" Rp. ").pop()).replace(/\D/g, '');
        if (value_shipping == false) {
            value_shipping = "0";
        }
        let rp_shipping = parseInt(count_price) + parseInt(value_shipping);
        $('#rp_shipping').html('Rp. ' + value_shipping.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $('#total_payment').html('Rp. ' + rp_shipping.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    })
    </script>

</body>

</html>