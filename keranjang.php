<?php 
session_start();

$koneksi = new mysqli("localhost","root","","petshop");
if (empty($_SESSION["keranjang"]) OR !isset($_SESSION["keranjang"])) 
{
    echo "<script>alert('keranjang masih kosong')</script>";
    echo "<script>location='index.php';</script>";
}

// cek stok produk sebelum cekout
if (isset($_POST['btn_checkout'])) {
    unset($_SESSION['checkout']);
    $daftar_co = $_POST['daftar_co'];
    $stokHabis = [];

    if (empty(trim($daftar_co))) {
        echo "<script>alert('Silahkan pilih barang yang ingin dicheckout')</script>";
    } else {
        foreach (explode("-", $daftar_co) as $value) {
            $_SESSION['checkout'][$value] = $_SESSION['keranjang'][$value];
        }

        foreach ($_SESSION['checkout'] as $id_produk => $jumlah) {
            $cekStok = $koneksi->query("SELECT namaproduk,stok FROM produk WHERE idproduk = {$id_produk}")->fetch_assoc();
    
            if ($jumlah > $cekStok['stok']) {
                $stokHabis[] =  "Stok untuk " . $cekStok['namaproduk'] . " tersisa " . $cekStok['stok'];
            }
        }
    
        if (empty($stokHabis)) {
            echo "<script>location='checkout.php';</script>";
        } else {
            foreach ($stokHabis as $habis) {
                echo "<script>alert('$habis')</script>";
            }
        }
    }
}

if (isset($_POST['ubah_btn'])) {
    $jenis = $_POST['ubah_btn']; // tambah atau kurang
    $idProduk = $_POST['ubah_id']; // id produk
    $cekStok = $koneksi->query("SELECT namaproduk,stok FROM produk WHERE idproduk = {$idProduk}")->fetch_assoc();

    if ($jenis == 'tambah') {
        $jml = $_SESSION['keranjang'][$idProduk];
        if (($jml + 1) > $cekStok['stok']) {
            echo "<script>alert('Stok untuk ". $cekStok['namaproduk'] ." tersisa " . $cekStok['stok'] . "')</script>";
        } else {
            $_SESSION['keranjang'][$idProduk] += 1;
        }
    } else if ($jenis == 'kurang') {
        if ($_SESSION['keranjang'][$idProduk] > 1) {
            $_SESSION['keranjang'][$idProduk] -= 1;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">



</head>

<body>
    <?php require_once 'include/header.php'; ?>

    <section class="section-cart my-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="main-heading">
                        <h1>Keranjang</h1>
                        <div class="line1"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col" class="text-center">Produk</th>
                            <th scope="col" class="text-center">Price</th>
                            <th scope="col" class="text-center">Quantity</th>
                            <th scope="col" class="text-center">Total</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $totalbelanja=0;
                            foreach ($_SESSION["keranjang"] as $idproduk => $jumlah){
                            $ambil = $koneksi->query("SELECT * FROM produk WHERE idproduk='$idproduk'");
                            $pecah = $ambil->fetch_assoc();
                            if($pecah['discount'] !== 0 || $pecah['discount'] !== null){
                                $discount = $pecah["hargaproduk"] - $pecah["hargaproduk"] * $pecah["discount"];
                            }else{
                                $discount = number_format($pecah["hargaproduk"]);
                            }
                            $subharga = $discount*$jumlah;
                            $totalbelanja+=$subharga;
					    ?>
                        <tr>
                            <td>
                                <input type="checkbox" style="transform: scale(2);" name="pilih_co" onchange="piliCoChange(<?= $idproduk ?>)">
                            </td>
                            <td class="td-img">
                                <div class="checkout-item">
                                    <img src="fotoproduk/<?php echo $pecah["fotoproduk"];  ?>" class="item-image"
                                        alt="">
                                    <div class="checkout-info">
                                        <p class="checkout-name"><?php echo $pecah["namaproduk"]; ?></p>
                                        <p>Rp. <?php echo number_format($discount); ?> x
                                            <?php echo $jumlah; ?></p>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center">Rp. <?php echo number_format($discount); ?></td>
                            <td class="text-center">
                                <form style="display: flex;justify-content: center;align-items: center" action="" method="POST">
                                    <button name="ubah_btn" value="kurang" class="btn btn-xs btn-success" style="padding: 4px">
                                        <i style="font-weight: bold;padding: 7px">-</i>
                                    </button>
                                    <p style="margin: 0 10px"><?php echo $jumlah; ?></p>
                                    <button name="ubah_btn" value="tambah" class="btn btn-xs btn-success" style="padding: 4px">
                                        <i style="font-weight: bold;padding: 5px">+</i>
                                    </button>
                                    <input type="hidden" name="ubah_id" value="<?= $idproduk ?>">
                                </form>
                            </td>
                            <td class="text-center">Rp. <?php echo number_format($subharga); ?></td>
                            <td class="text-center"><a href="hapuskeranjang.php?id=<?php echo $idproduk ?>"
                                    class="btn btn-sm btn-danger"><i class="bi bi-trash"></i></a></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4">
                            </td>
                            <td class="text-center">
                                TOTAL ITEM :
                            </td>
                            <td class="text-center">
                                Rp. <?php echo number_format($totalbelanja); ?>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>

            <div class="row text-center mt-5">
                <div class="col-12">
                    <div class="group-button" role="group" aria-label="Basic example">
                        <div onclick="window.location.href='index.php';" class="btn btn-outline-secondary
                        rounded-0 px-5">Kembali</div>
                        <form action="" method="POST" style="display: inline-block">
                            <input type="hidden" name="daftar_co" id="daftar_co" value="">
                            <button type="submit" class="btn btn-outline-secondary rounded-0 px-5" name="btn_checkout">Checkout</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php require_once 'include/footer.php'; ?>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <script>
        const krjSementara = [];

        function piliCoChange(e) {
            const cek = krjSementara.includes(e);
            if (!cek) {
                krjSementara.push(e);
            } else {
                krjSementara.splice(krjSementara.indexOf(e), 1);
            }
            const daftarCo = document.getElementById("daftar_co");
            daftarCo.value = krjSementara.join("-");
        }
    </script>

</body>

</html>