<?php 
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");

$idppelanggan = $_SESSION["pelanggan"]['idppelanggan'];
$ambil = $koneksi->query("SELECT * FROM pelanggan WHERE idppelanggan = '$idppelanggan'");
$arrpelanggan = $ambil->fetch_assoc();
// echo print_r($arrpelanggan['emailpelanggan']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Palagan Petshop - Profile</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">



</head>

<body>
    <?php require_once 'include/header.php'; ?>
    <section class="section-timezone ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 align-self-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Profile
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-4 text-center align-self-center">
                    <span>Jogja | <span id="date"></span> | <span id="time"></span></span>

                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </section>
    <!--konten-->
    <section class="section-profile py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="mb-3">
                                    <label class="form-label">Nama Lengkap</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $arrpelanggan['namapelanggan'] ?>" name="nama">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $arrpelanggan['emailpelanggan'] ?>" name="email">
                                </div>
                            </div>
                            <div class="col-md-5 offset-md-1">
                                <div class="mb-3">
                                    <label class="form-label">Nomor Telepon</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $arrpelanggan['telppelanggan'] ?>" name="telepon">
                                </div>
                                <div class="mb-3" style="position: relative;">
                                    <label class="form-label">Password</label>
                                    <input id="password-field" type="password" class="form-control"
                                        value="<?php echo $arrpelanggan['passwordpelanggan'] ?>" name="password">
                                    <span id="secretView" toggle="#password-field"
                                        class="bi bi-eye-fill toggle-password"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Alamat</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $arrpelanggan['alamatpelanggan'] ?>" name="alamat">
                                </div>
                                <div class="mb-3">
                                    <button name="ubah" class="btn btn-dark mt-2">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php
                        if (isset($_POST['ubah'])) 
                        { 
                            $nama    = $_POST['nama'];
                            $email   = $_POST['email'];
                            $telepon = $_POST['telepon'];
                            $password   = $_POST['password'];
                            $alamat     = $_POST['alamat'];
                            $idpl = $arrpelanggan['idppelanggan'];


                            $koneksi->query("UPDATE pelanggan SET 
                            namapelanggan='$nama',
                            emailpelanggan='$email',
                            telppelanggan='$telepon',
                            passwordpelanggan='$password',
                            alamatpelanggan='$alamat' 
                            WHERE idppelanggan='$idpl'");

                            echo "<div class='alert alert-success'>Update Profile Berhasil</div>";
                            echo "<meta http-equiv='refresh' content='1;url=profile.php'>"; 
                        }
                        ?>
                </div>
            </div>
        </div>
    </section>
    <style>
    span.toggle-password {
        position: absolute;
        right: 0;
        top: 50%;
    }
    </style>
    <?php require_once 'include/footer.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/app.js?<?php echo rand()?>"></script>

    <script>
    $("#secretView").click(function() {
        $(this).toggleClass("bi bi-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    </script>
</body>

</html>