<?php
session_start();
//Koneksi database
$koneksi = new mysqli("localhost","root","","petshop");

$idppelanggan = $_SESSION["pelanggan"]['idppelanggan'];
$ambil = $koneksi->query("SELECT * FROM pelanggan WHERE idppelanggan = '$idppelanggan'");
$arrpelanggan = $ambil->fetch_assoc();

// $idpembelian = '';
$getpembelian = $koneksi->query("SELECT * FROM pembelian");
$arrpembelian = $getpembelian->fetch_assoc();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bukti Pembayaran</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?<?php echo rand()?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css?<?php echo rand()?>">
</head>
<style>
html,
body {
    height: 100%;
}
</style>

<body class="body-upload d-flex justify-content-center align-items-center">
    <div class="section-login section-upload">
        <div class="login-page">
            <div class="login-logo text-center">
                <img src="assets/img/logo/logo.png" class="img-fluid w-25" alt=""
                    onclick="window.location.href='index.php';" style="cursor:pointer;">
            </div>
            <div class="form">
                <form class="login-form" method="post" enctype="multipart/form-data">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-3">Upload Bukti Pembayaran</label>
                                    <div class="preview-zone hidden">
                                        <div class="box box-solid">
                                            <div class="box-header with-border">
                                                <div><b>Preview</b></div>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-danger btn-xs remove-preview">
                                                        <i class="bi bi-arrow-repeat"></i> Ulangi
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body"></div>
                                        </div>
                                    </div>
                                    <div class="dropzone-wrapper">
                                        <div class="dropzone-desc">
                                            <i class="bi bi-cloud-download-fill"></i>
                                            <p>Choose an image file or drag it here.</p>
                                        </div>
                                        <input type="file" name="foto" class="dropzone">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <button type="submit" name="upload" class="btn btn-primary pull-right">Upload</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
	if (isset($_POST['upload'])) 
    {
        
        $nama = $_FILES['foto']['name'];
        $lokasi = $_FILES['foto']['tmp_name'];
        move_uploaded_file($lokasi, "invoice/".$nama);

        $idpl = $arrpelanggan['idppelanggan'];
        $namapelanggan = $arrpelanggan['namapelanggan'];
        $tanggal = date("Y-m-d");

        $idpembelian = $arrpembelian['idpembelian'];
        $no_order = $arrpembelian['no_order'];



    
        
        $koneksi->query("INSERT INTO invoice (idppelanggan, idpembelian, no_order, namapelanggan, file_name, tanggal) VALUES ( '$idpl', '$idpembelian', '$no_order', '$namapelanggan', '$nama', '$tanggal' )");
    
        echo "<div class='alert alert-info'>Kami akan segera memeriksa pembayaran anda</div>";
        // echo "<script>location='upload.php';</script>";

    }
    ?>






    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script>
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                var htmlPreview =
                    '<img width="200" src="' + e.target.result + '" />' +
                    '<p>' + input.files[0].name + '</p>';
                var wrapperZone = $(input).parent();
                var previewZone = $(input).parent().parent().find('.preview-zone');
                var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                wrapperZone.removeClass('dragover');
                previewZone.removeClass('hidden');
                boxZone.empty();
                boxZone.append(htmlPreview);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function reset(e) {
        e.wrap('<form>').closest('form').get(0).reset();
        e.unwrap();
    }

    $(".dropzone").change(function() {
        readFile(this);
    });

    $('.dropzone-wrapper').on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('dragover');
    });

    $('.dropzone-wrapper').on('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('dragover');
    });

    $('.remove-preview').on('click', function() {
        var boxZone = $(this).parents('.preview-zone').find('.box-body');
        var previewZone = $(this).parents('.preview-zone');
        var dropzone = $(this).parents('.form-group').find('.dropzone');
        boxZone.empty();
        previewZone.addClass('hidden');
        reset(dropzone);
    });
    </script>
</body>

</html>